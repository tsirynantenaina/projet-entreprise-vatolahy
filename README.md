# Concepotion et réalisation d'une application de gestion de vente (Framework php CodeIgniter 3)
J'ai développé l'application pour l'entreprise Vatolahy Fianarantsoa; une entreprise qui se base sur la vente des betons pré-fabriqués.


## But du projet
Le but du projet est de :
* Permettre au gérant de bien gérer l’entreprise
* Lui aider à évaluer l’amélioration et la progression de son entreprise
* Créer une application qui facilite et accélère le travail 


## Fonctionnalités
Alors on peut dire que notre application contient des fonctionnalités comme ceci :
* Afficher les clients 
* La variation annuelle, mensuelle, journalière de chiffre d’affaire
* Les produits en stock
* Création d’achat et commande (journalier, mensuel, annuel)
* Affichage achat journalier, mensuel, annuel
* Affichage de la commande en attente, livré 
* Affichage de commandes payées
* Affichage des avances et des restes à payer
* Enregistrent des dépenses
* Enregistrement automatiques des comptes de l’entreprise
* Imploration des données en Excel


## Prérequis et installation
* Framework codeigniter 3
* L'application necessite: php version 7 et serveur apache
* Une base deonnées Mysql pour stocker les donnees



## Importation de base de Données  
* Imporetr la base de données BD.sql 
* Veuillez entrer dans le dossier "Application", puis dans dossier "config" et ouvrez le dossier database.php

```
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'port' => '3308',
	'username' => 'root',
	'password' => '',
	'database' => 'DB',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

```
* Verifier bien votre port
```
'port' => '3308',

```

## Commant lancer l'application?
* Coller le dossier dans votre repertoire www
* lancer l'application

## Authentification Par defaut
* Utilisateur:
```
admin

```
* Mot de passe:
```
admin

```

