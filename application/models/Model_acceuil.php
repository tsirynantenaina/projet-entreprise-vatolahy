<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_acceuil extends CI_Model {

    public function nombreClient(){
		$nombre = 0 ;
		$requete = $this->db->query('SELECT count(*) as compte  from client');
	    $res = $requete->row();
	     if (isset($res->compte) AND $res->compte != NULL){
	     	$nombre = $res->compte ; 
	     }
	     return $nombre;
	}

	public function nombreProduit(){
		$nombre = 0 ;
		$requete = $this->db->query('SELECT count(*) as compte  from produit');
	    $res = $requete->row();
	     if (isset($res->compte) AND $res->compte != NULL){
	     	$nombre = $res->compte ; 
	     }
	     return $nombre;
	}

	public function nombreCommandeAttente(){
		$nombre = 0 ;
$requete = $this->db->query('SELECT count(*) as commandeAttente  from commande where emargement ="en attente" ');
	    $res = $requete->row();
	     if (isset($res->commandeAttente) AND $res->commandeAttente != NULL){
	     	$nombre = $res->commandeAttente ; 
	     }
	     return $nombre;
	}

	public function nombreLivraison(){
		$nombre = 0 ;
      $requete = $this->db->query('SELECT count(*) as liv  from commande where emargement ="en attente" AND dateLivraison ="'.date('Y-m-d').'" ');
	    $res = $requete->row();
	     if (isset($res->liv) AND $res->liv != NULL){
	     	$nombre = $res->liv ; 
	     }
	     return $nombre;
	}

	public function commandeJour(){
		$nombre = 0 ;
        $requete = $this->db->query('SELECT count(*) as liv  from commande where dateCom like "%'.date('Y-m-d').'%" ');
	    $res = $requete->row();
	     if (isset($res->liv) AND $res->liv != NULL){
	     	$nombre = $res->liv ; 
	     }
	     return $nombre;
	}

	public function achatJour(){
		$nombre = 0 ;
        $requete = $this->db->query('SELECT sum(qteAchat) as liv  from achat where dateAchat like "%'.date('Y-m-d').'%" ');
	    $res = $requete->row();
	     if (isset($res->liv) AND $res->liv != NULL){
	     	$nombre = $res->liv ; 
	     }
	     return $nombre;
	}

	public function entrer($annee , $mois){
		$nombre = 0 ;
        $requete = $this->db->query('SELECT sum(sommeEntre) as entre  from compte where dateCompte like "%'.$annee.'-'.$mois.'%" ');
	    $res = $requete->row();
	     if (isset($res->entre) AND $res->entre != NULL){
	     	$nombre = $res->entre ; 
	     }
	     return $nombre;
	}
	public function sortir($annee , $mois){
		$nombre = 0 ;
        $requete = $this->db->query('SELECT sum(sommeCompte) as sortir  from compte where dateCompte like "%'.$annee.'-'.$mois.'%" ');
	    $res = $requete->row();
	     if (isset($res->sortir) AND $res->sortir != NULL){
	     	$nombre = $res->sortir ; 
	     }
	     return $nombre;
	}

	
}
