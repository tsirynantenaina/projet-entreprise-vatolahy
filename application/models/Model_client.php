<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_client extends CI_Model{
	public function listeclient(){
		$query = $this->db->query('SELECT * FROM client ');
		return $query->result();
		
	}

	public function insertclient($nom , $adresse , $telephone ){
	 	$data = array(
	 		'nomCli' => $nom,
	 		'adresseCli' => $adresse,
	 		'telephoneCli' =>$telephone,
	 		);
	 	$this->db->insert('client' , $data);
	 }

	public function actualiser(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM client ');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idCli.'</td>
                        <td>'.$res->nomCli.'</td>
                        <td>'.$res->adresseCli.'</td>
                        <td>'.$res->telephoneCli.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idCli.'" data-nom="'.$res->nomCli.'" data-adresse="'.$res->adresseCli.'" data-telephone="'.$res->telephoneCli.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editCli"><i class="glyphicon glyphicon-pencil"></i></button>
                           <button type="button" data-id="'.$res->idCli.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}

	 public function editclient($id , $nom , $adresse , $telephone ){
	 	$data = array(
	 		'nomCli' => $nom,
	 		'adresseCli' => $adresse,
	 		'telephoneCli' =>$telephone,
	 		);
	 	$this->db->where('idCli' , $id);
	 	$this->db->update('client' , $data) ; 
	 }

	 public function supprimeclient($id){
	 	$this->db->where('idCli', $id);
	 	$this->db->delete('client');
	 }


	 public function search($saisi){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM client where idCli like "%'.$saisi.'%" OR nomCli like "%'.$saisi.'%" OR adresseCli like "%'.$saisi.'%" OR telephoneCli like "%'.$saisi.'%"');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idCli.'</td>
                        <td>'.$res->nomCli.'</td>
                        <td>'.$res->adresseCli.'</td>
                        <td>'.$res->telephoneCli.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idCli.'" data-nom="'.$res->nomCli.'" data-adresse="'.$res->adresseCli.'" data-telephone="'.$res->telephoneCli.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editCli"><i class="glyphicon glyphicon-pencil"></i></button>
                           <button type="button" data-id="'.$res->idCli.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';			
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}

	

}