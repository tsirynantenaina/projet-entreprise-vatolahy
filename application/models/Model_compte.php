<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_compte extends CI_Model {
	
	function compte($annee , $mois){
		$query = $this->db->query('SELECT * from compte where dateCompte like "'.$annee.'-'.$mois.'%" order by dateCompte ASC ');
	 	return $query->result();
	}

	function sommeEntre($annee , $mois){
		$query = $this->db->query('SELECT  sum(sommeEntre) as sommeEntre from compte where dateCompte like "'.$annee.'-'.$mois.'%" ');
	 	$res = $query->row();
	 	$entrer=0;
	    if (isset($res->sommeEntre) AND $res->sommeEntre != NULL){
	     	$entrer = $res->sommeEntre ; 
	    }
	    return $entrer;
	}

	function sommeSortie($annee , $mois){
		$query = $this->db->query('SELECT  sum(sommeCompte) as sommeSortie from compte where dateCompte like "'.$annee.'-'.$mois.'%" ');
	 	$res = $query->row();
	 	$sortir=0;
	    if (isset($res->sommeSortie) AND $res->sommeSortie != NULL){
	     	$sortir = $res->sommeSortie; 
	    }
	    return $sortir;
	}

	function sommeTotalSortie($annee ){
		$query = $this->db->query('SELECT  sum(sommeCompte) as sommeSortie from compte where dateCompte like "'.$annee.'%" ');
	 	$res = $query->row();
	 	$sortir=0;
	    if (isset($res->sommeSortie) AND $res->sommeSortie != NULL){
	     	$sortir = $res->sommeSortie; 
	    }
	    return $sortir;
	}

	function sommeTotalEntre($annee ){
		$query = $this->db->query('SELECT  sum(sommeEntre) as sommeEntre from compte where dateCompte like "'.$annee.'%" ');
	 	$res = $query->row();
	 	$entrer=0;
	    if (isset($res->sommeEntre) AND $res->sommeEntre != NULL){
	     	$entrer = $res->sommeEntre ; 
	    }
	    return $entrer;
	}


}