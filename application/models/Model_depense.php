<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_depense extends CI_Model {
	
	public function listeDepense(){
		$query = $this->db->query('SELECT * FROM compte where action ="DEPENSE" ORDER BY dateCompte');
		return $query->result();	
	}

	public function insertDepense($dateCompte , $action , $operation , $somme ){
	 	$data = array(
	 		'dateCompte'=> $dateCompte,
	 		'action' => $action,
	 		'operation' => $operation,
	 		'sommeCompte' =>$somme,
	 		);
	 	$this->db->insert('compte' , $data);
	 }

	 public function editDepense($id , $operation , $somme){
	 	$data = array(
	 		'operation' => $operation,
	 		'sommeCompte' => $somme,
	 		);
	 	$this->db->where('idCompte' , $id);
	 	$this->db->update('compte' , $data) ; 
	 }


	 public function actualiser(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM compte where action ="DEPENSE" order by dateCompte');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->dateCompte.'</td>
                        <td>'.$res->operation.'</td>
                        <td>'.$res->sommeCompte.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idCompte.'" data-operation="'.$res->operation.'" data-somme="'.$res->sommeCompte.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editDepense"><i class="fa fa-pencil"></i></button>
                           <button type="button" data-id="'.$res->idCompte.'" data-operation="'.$res->operation.'" data-somme="'.$res->sommeCompte.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement de dépense</td></tr>';
	    }

	    echo $resultat; 
	}


	 public function supprimeDepense($id){
	 	$this->db->where('idCompte', $id);
	 	$this->db->delete('compte');
	 }

	 public function search($saisi){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM compte where (dateCompte like "%'.$saisi.'%" OR operation like "%'.$saisi.'%" OR sommeCompte like "%'.$saisi.'%") AND  action ="DEPENSE"  ORDER BY dateCompte');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->dateCompte.'</td>
                        <td>'.$res->operation.'</td>
                        <td>'.$res->sommeCompte.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idCompte.'" data-operation="'.$res->operation.'" data-somme="'.$res->sommeCompte.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editDepense"><i class="fa fa-pencil"></i></button>
                           <button type="button" data-id="'.$res->idCompte.'" data-operation="'.$res->operation.'" data-somme="'.$res->sommeCompte.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement de dépense</td></tr>';
	    }

	    echo $resultat; 
	}

}