<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_produit extends CI_Model{
	public function listeproduit(){
		$query = $this->db->query('SELECT * FROM produit ');
		return $query->result();
		
	}

	public function listeReap(){
		$query = $this->db->query('SELECT * FROM produit where stock < 10 ');
		return $query->result();
		
	}

	public function insertproduit($designation , $stock, $pu ){
	 	$data = array(
	 		'designation' => $designation,
	 		'stock' => $stock,
	 		'pu' =>$pu,
	 		);
	 	$this->db->insert('produit' , $data);
	 }

	public function actualiser(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM produit');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idProd.'" data-designation="'.$res->designation.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd"><i class="glyphicon glyphicon-pencil"></i></button>
                           <button type="button" data-id="'.$res->idProd.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}
	public function actualiserReap(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM produit where stock < 10');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                           <a class="btn btn-primary" href="<?php echo base_url(\'Controller_reaprov\');?>">Raprovisionner</a>  
  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}

	 public function editproduit($id , $designation , $stock, $pu ){
	 	$data = array(
	 		'designation' => $designation,
	 		'stock' => $stock,
	 		'pu' =>$pu,
	 		);
	 	$this->db->where('idProd' , $id);
	 	$this->db->update('produit' , $data) ; 
	 }

	 public function supprimeproduit($id){
	 	$this->db->where('idProd', $id);
	 	$this->db->delete('produit');
	 }


	 public function search($saisi){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM Produit where idProd like "%'.$saisi.'%" OR designation like "%'.$saisi.'%" OR stock like "%'.$saisi.'%" OR pu like "%'.$saisi.'%"');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idProd.'" data-designation="'.$res->designation.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd"><i class="glyphicon glyphicon-pencil"></i></button>
                           <button type="button" data-id="'.$res->idProd.'"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}



	 public function searchReap($saisi){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM Produit where (idProd like "%'.$saisi.'%" OR designation like "%'.$saisi.'%" OR stock like "%'.$saisi.'%" OR pu like "%'.$saisi.'%") AND stock < 10');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                        	<a class="btn btn-primary" href="<?php echo base_url(\'Controller_reaprov\');?>">Raprovisionner</a>  
 
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}


}