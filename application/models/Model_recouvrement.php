<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_recouvrement extends CI_Model{
	public function listeAchat($annee){
		$query = $this->db->query('SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where dateAchat like "'.$annee.'%" AND achat.idCli = client.idCli group by achat.factureAchat ORDER by achat.factureAChat ASC ');
	 	return $query->result();
	}

	public function listeCommande($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    return $query->result();
  }

  public function totalAchat($annee){
  		$tot = 0 ; 
		$query = $this->db->query('SELECT sum(subtotalAchat) as total from achat where dateAchat like "'.$annee.'%"');
	 	$res =  $query->row();
	 	if (isset($res->total) AND $res->total != NULL){
	     	$tot = $res->total ; 
	     }
	     return $tot;
	}

	public function totalMontant($annee){
		$tot = 0 ;
		$query = $this->db->query('SELECT sum(Net) as montantTotal  from commande where dateCom like "'.$annee.'%"');
	 	$res =  $query->row();
	 	if (isset($res->montantTotal) AND $res->montantTotal != NULL){
	     	$tot = $res->montantTotal ; 
	     }
	     return $tot;
	}

	public function totalAvance($annee){
		$tot = 0 ;
		$query = $this->db->query('SELECT sum(avance) as avanceTotal  from commande where dateCom like "'.$annee.'%"');
	 	$res =  $query->row();
	 	if (isset($res->avanceTotal) AND $res->avanceTotal != NULL){
	     	$tot = $res->avanceTotal ; 
	     }
	     return $tot;
	}
	public function totalReste($annee){
		$tot = 0 ;
		$query = $this->db->query('SELECT sum(reste) as resteTotal  from commande where dateCom like "'.$annee.'%"');
	 	$res =  $query->row();
	 	if (isset($res->resteTotal) AND $res->resteTotal != NULL){
	     	$tot = $res->resteTotal ; 
	     }
	     return $tot;
	}

	public function sommeRecu($annee){
		$tot = 0 ;
		$query = $this->db->query('SELECT sum(sommeRecu) as recuTotal  from commande where dateCom like "'.$annee.'%"');
	 	$res =  $query->row();
	 	if (isset($res->recuTotal) AND $res->recuTotal != NULL){
	     	$tot = $res->recuTotal ; 
	     }
	     return $tot;
	}

	public function searchAchat($annee){
    $query = $this->db->query('SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where dateAchat like "'.$annee.'%" AND achat.idCli = client.idCli group by achat.factureAchat ORDER by achat.factureAChat ASC ');
	$resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++; 
          $resultat .= ' <tr> 
                          <td>'.$ligne.'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>'.$res->telephoneCli.'</td>
                          <td>'.$res->adresseCli.'</td>
                          <td>Fact achat N° '.$res->factureAchat.'</td>
                          <td>'.$res->dateAchat.'</td>
                          <td>'.$res->net.'</td>
                          ';

                          
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="11" style="padding-left:450px">Aucun resultat </td></tr>';
      }

      echo $resultat; 
  
  }


  public function searchCom($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;$couleur = "";
          $payement =$res->emargement;
          if($payement=="LIVRE-NON PAYE"){
            $couleur = "yellow";
          }
          
          $resultat .= ' <tr>
          				  <td>'.$ligne.'</td> 
                          <td>'.$res->telephoneCli.'</td>
                          <td>'.$res->adresseCli.'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>Fact commande N°'.$res->idCom.'</td>
                          <td>'.date('Y-m-d',strtotime($res->dateCom)).'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->reste.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->emargement.'</td>
                          <td>'.$res->jourLivrance.'</td>
                          <td>'.$res->sommeRecu.'</td>
                          </tr>';
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="12" style="padding-left:450px">Aucune commande </td></tr>';
      }

      echo $resultat; 
  
  }

  
}
	