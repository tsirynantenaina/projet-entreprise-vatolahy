<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_reaprov extends CI_Model{
	public function listeproduit(){
		$query = $this->db->query('SELECT * FROM produit ');
		return $query->result();
		
	}

	public function reaprov($id , $qteReaprov){
	 $query = $this->db->query('UPDATE produit SET stock = (stock + '.$qteReaprov.')  where idProd='.$id);
	}

	public function actualiser(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM produit');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button type="button" data-id="'.$res->idProd.'" data-designation="'.$res->designation.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd">
                           		Reaprovisionner
                           </button>
                           
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}


	 public function search($saisi){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM Produit where idProd like "%'.$saisi.'%" OR designation like "%'.$saisi.'%" OR stock like "%'.$saisi.'%" OR pu like "%'.$saisi.'%"');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                            <button type="button" data-id="'.$res->idProd.'" data-designation="'.$res->designation.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd">
                           		Reaprovisionner
                           </button> 
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}

}