<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_commande extends CI_Model {
	
	public function listeclient(){
    $query = $this->db->query('SELECT * FROM client order by idCli DESC');
    return $query->result();
  }

  public function listeCommande($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    return $query->result();
  }
  public function insertcommande($idCli , $commande , $dateLiv , $montant , $avance , $reste , $emargement){
    $data = array(
      'idCli' => $idCli,
      'commande' => $commande,
      'dateLivraison' =>$dateLiv,
      'Net' =>$montant, 
      'avance' => $avance , 
      'sommeRecu' => $avance , 
      'reste' => $reste , 
      'emargement' => $emargement , 
      );
    $this->db->insert('commande' , $data);

    $facture=0 ; 
    $queryFacture =  $this->db->query('SELECT max(idCom) as dernierFacture from commande');
    $res = $queryFacture->row();
      if(isset ($res->dernierFacture) && $res->dernierFacture !=NULL){
           $facture =  $res->dernierFacture ; 
      }else{
      $facture = 0 ;  
    }

    $action='COMMANDE (avance)' ; 
     $operation = 'PAYEMENT AVANCE COMMANDE';
      $dateCompte = date('Y-m-d');
      $factureCommande ='commande N°'.$facture ; 
      $data = array(
        'action' =>$action,
        'operation' => $operation ,
        'dateCompte' =>$dateCompte ,
        'sommeEntre' => $avance, 
        'factureEntre' => $factureCommande ,
      );
      $this->db->insert('compte' , $data);


   }


  public function actualiseCommande($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;
          $resultat .= ' <tr> <td>'.date("Y-m-d",strtotime($res->dateCom)).'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.date("d-m-Y ",strtotime($res->dateLivraison)).'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td>'.$res->reste.'</td>
                          <td>'.$res->emargement.'</td>
                          <td>'.$res->idCom.'</td>
                          <td>
                            <div class="btn-group" role="group" aria-label="...">                         
                              <button data-idcom ="'.$res->idCom.'" data-idcli ="'.$res->idCli.'" data-commande ="'.$res->commande.'" data-livraison ="'.$res->dateLivraison.'" data-net ="'.$res->Net.'"  data-avance="'.$res->avance.'" data-reste="'.$res->reste.'" title="modifier" type="button"  data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editCom" ><i class="fa fa-pencil"></i></button>
                              <button data-nom ="'.$res->nomCli.'" data-idcom="'.$res->idCom.'" title="annuler commande" type="button"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="fa fa-close"></i></button>
                             </div> 
                          </td> 
                          <td>
                          <button data-idcom ="'.$res->idCom.'" data-nom="'.$res->nomCli.'" data-montant="'.$res->Net.'" data-avance="'.$res->avance.'" data-reste="'.$res->reste.'" class="btn btn-primary livraison" title="liver commande" data-toggle="modal" data-target="#livrer">livré</button>
                           </td>

                          </tr>';
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="11" style="padding-left:450px">Aucune commande en attente</td></tr>';
      }

      echo $resultat; 
  
  }


    public function getClient($idCli){
      $query = $this->db->query('SELECT * from client ');
      $resultat = '';
      foreach ($query->result() as $res) {
        if($res->idCli == $idCli){
            $resultat .= '<option selected value="'.$res->idCli.'">'.$res->nomCli.'</option>';
        }else{
           $resultat .= '<option value="'.$res->idCli.'">'.$res->nomCli.'</option>';
        }
      }
      echo $resultat; 
    }


   
  
    
    public function editCommande($idCom , $idCli , $commande , $dateLiv , $montant , $avance , $reste , $emargement){
    $data = array(
      'idCli' => $idCli,
      'commande' => $commande,
      'dateLivraison' =>$dateLiv,
      'Net' =>$montant, 
      'avance' => $avance , 
      'reste' => $reste , 
      'emargement' => $emargement , 
      );
     $this->db->where('idCom' , $idCom);
     $this->db->update('commande' , $data);

      //modification table compte
      $this->db->query('UPDATE compte set sommeEntre = '.$avance.' where action = "COMMANDE (avance)" AND factureEntre = "commande N°'.$idCom.'"');
      



   }


  public function livraison($idCom , $mode , $montant , $avance , $reste ){
      if($mode =='LIVRE-PAYE'){
          $reste = 0 ; 
          $data = array(
          'Net' =>$montant, 
          'avance' => $avance, 
          'reste' => $reste , 
          'emargement' => $mode , 
          'sommeRecu' => $montant , 
          'jourLivrance' => date('Y-m-d'),
          );
         $this->db->where('idCom' , $idCom);
         $this->db->update('commande' , $data) ;
         
          //payement reste
         $rest = $montant - $avance ; 
         $action='COMMANDE (reste)' ; 
         $operation = 'PAYEMENT RESTE COMMANDE';
         $dateCompte = date('Y-m-d');
         $factureCommande ='commande N°'.$idCom.''; 
         $data = array(
            'action' =>$action,
            'dateCompte' =>$dateCompte ,
            'operation' => $operation , 
            'sommeEntre' => $rest, 
            'factureEntre' => $factureCommande ,
         );
         $this->db->insert('compte' , $data);

      }else if($mode =='LIVRE-NON PAYE'){
          $data = array(
          'Net' =>$montant, 
          'avance' => $avance, 
          'sommeRecu' => $avance,
          'reste' => $reste , 
          'emargement' =>$mode, 
          'jourLivrance' => date('Y-m-d'),
          );
         $this->db->where('idCom' , $idCom);
         $this->db->update('commande' , $data) ; 

      //modification table compte
      $this->db->query('UPDATE compte set sommeEntre = '.$avance.' where action = "COMMANDE (avance)" AND factureEntre = "commande N°'.$idCom.'"');
     
      }
  }


  public function actualisePayer($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;
          $resultat .= ' <tr><td> livré à '.$res->nomCli.' le '.date("d-m-Y ",strtotime($res->jourLivrance)).'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->emargement.'</td>
                          <td>'.$res->sommeRecu.'</td>
                          <td>'.$res->idCom.'</td>
                          </tr>';
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="6" style="padding-left:450px">Aucun commande livrée et payé</td></tr>';
      }

      echo $resultat; 
  
  }


    public function actualiseNonPayer($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;
          $resultat .= ' <tr> <td>livré à '.$res->nomCli.' le '.date("d-m-Y ",strtotime($res->jourLivrance)).'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td>'.$res->reste.'</td>
                          <td>'.$res->emargement.'</td>
                          <td>'.$res->idCom.'</td>
                          <td>
                              <button data-idcom="'.$res->idCom.'" data-net="'.$res->Net.'" data-reste="'.$res->reste.'" data-nom="'.$res->nomCli.'" class="btn btn-primary regler" title="totalité payement regler" data-toggle="modal" data-target="#regler">reglé
         
                          </td>
                          </tr>';
                                        
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="10" style="padding-left:450px">Aucun commande livrée et payé</td></tr>';
      }
      echo $resultat; 
  
  }


  public function annulation($idCom , $emargement){
      $data = array(
          'emargement' =>$emargement, 
          );
         $this->db->where('idCom' , $idCom);
         $this->db->update('commande' , $data) ; 
  }

  public function actualiseDecom($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;
          $resultat .= ' <tr> <td>'.date("d-m-Y ",strtotime($res->dateCom)).'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.date("d-m-Y ",strtotime($res->dateLivraison)).'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td>'.$res->reste.'</td>
                          <td>'.$res->emargement.'</td>
                          <td>'.$res->idCom.'</td>
                          <td>
                          <button data-idcom ="'.$res->idCom.'" data-nom="'.$res->nomCli.'" data-montant="'.$res->Net.'" data-avance="'.$res->avance.'" data-reste="'.$res->reste.'" class="btn btn-primary livraison" title="liver commande" data-toggle="modal" data-target="#livrer">????</button>
                           </td>

                          </tr>';
                                        
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="10" style="padding-left:450px">Aucun commande livrée et payé</td></tr>';
      }
      echo $resultat; 
  
  }


    public function regulation($idCom , $emargement , $net ,$reste){
      $rest = 0 ;//deja regler donc reste à payer = 0 
      $data = array(
           'reste' => $rest, 
           'sommeRecu' => $net, 
          'emargement' =>$emargement, 
          );
         $this->db->where('idCom' , $idCom);
         $this->db->update('commande' , $data) ; 

         $action='COMMANDE (reste)' ; 
         $dateCompte = date('Y-m-d');
         $operation = 'PAYEMENT RESTE COMMANDE';
         $factureCommande ='commande N°'.$idCom.''; 
          $data = array(
            'action' =>$action,
            'dateCompte' =>$dateCompte ,
            'operation' => $operation, 
            'sommeEntre' => $reste, 
            'factureEntre' => $factureCommande ,
         );
         $this->db->insert('compte' , $data);


  }


   public function actualiseTous($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;$couleur = "";
          $payement =$res->emargement;
          if($payement=="LIVRE-NON PAYE"){
            $couleur = "yellow";
          }
          
          $resultat .= ' <tr> 
                          <td>'.$res->idCom.'</td>
                          <td>'.date("Y-m-d",strtotime($res->dateCom)).'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.date("d-m-Y ",strtotime($res->dateLivraison)).'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->reste.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->emargement.'</td>
                          <td>'.$res->jourLivrance.'</td>
                          <td>'.$res->sommeRecu.'</td>
                          </tr>';
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="11" style="padding-left:450px">Aucune commande </td></tr>';
      }

      echo $resultat; 
  
  }

  public function actualiseFact($condition){
    $query = $this->db->query('SELECT client.* ,commande.* FROM client , commande where '.$condition);
    $resultat = '';
    $ligne = 0 ;
    foreach ($query->result() as $res) {
          $ligne++;$couleur = "";
          $payement =$res->emargement;
          if($payement=="LIVRE-NON PAYE"){
            $couleur = "yellow";
          }
          
          $resultat .= ' <tr> 
                          <td>'.$res->idCom.'</td>
                          <td>'.date("Y-m-d",strtotime($res->dateCom)).'</td>
                          <td>'.$res->nomCli.'</td>
                          <td>'.$res->commande.'</td>
                          <td>'.date("d-m-Y ",strtotime($res->dateLivraison)).'</td>
                          <td>'.$res->Net.'</td>
                          <td>'.$res->avance.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->reste.'</td>
                          <td style="background-color:'.$couleur.'">'.$res->emargement.'</td>
                          <td>'.$res->jourLivrance.'</td>
                          <td>'.$res->sommeRecu.'</td>
                          <td><a class="btn btn-primary">Facture</a></td>
                          </tr>';
        
      }
      if($ligne==0){
       $resultat .= '<tr><td colspan="11" style="padding-left:450px">Aucune commande </td></tr>';
      }

      echo $resultat; 
  
  }
}



		
