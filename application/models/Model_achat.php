<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_achat extends CI_Model{
	public function listeAchat(){
		$query = $this->db->query('SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where achat.idCli = client.idCli group by achat.factureAchat ORDER by achat.factureAChat ASC ');
	 	return $query->result();
	}

	public function listeproduit(){
		$query = $this->db->query('SELECT * FROM produit');
	 	return $query->result();
	}
	public function listeclient(){
		$query = $this->db->query('SELECT * FROM client order by idCli DESC');
	 	return $query->result();
	}

	public function listepanier(){
		$query = $this->db->query('SELECT produit.* , panier.*  from panier , produit where produit.idProd = panier.idProd');
	 	return $query->result();
	}

	public function insertpanier($idProd , $quantite , $subtotal ){
	 	$query= $this->db->query('SELECT * from panier where idProd ='.$idProd);
		$res = $query->row();
		if(isset($res)){
			$this->db->query('UPDATE panier set qtePanier = (qtePanier +'.$quantite.') , subtotalPanier = (subtotalPanier +'. $subtotal.') where idProd = '.$idProd.'' ) ;
		}else{
			$data = array(
	 		'idProd' => $idProd,
	 		'qtePanier' => $quantite, 
	 		'subtotalPanier' => $subtotal,
	 		);
	 	   $this->db->insert('panier' , $data);
		}
	 }

	public function actualiseproduit(){
		$resultat = '';
		$ligne = 0 ;
		$query = $this->db->query('SELECT * FROM produit');
		foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '
					<tr>
                        <td>'.$res->idProd.'</td>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->stock.'</td>
                        <td>'.$res->pu.'</td>
                        <td><input style="width:70px" type="number" value="0" class="quantite" id="'.$res->idProd.'"></td>
                        <td>
                           <div class="btn-group" role="group" aria-label="...">                         
                            <button type="button" data-id="'.$res->idProd.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'"  title="ajouter panier" class="addCart" >Acheter </button>
                          </div>  
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
	    }

	    echo $resultat; 
	}

	public function actualisepanier(){
		$resultat = '';
		$ligne = 0 ;
		$net = 0 ;
		$query = $this->db->query('SELECT produit.* , panier.* from panier , produit where produit.idProd = panier.idProd');
	 	foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '
					<tr>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->qtePanier.'</td>
                        <td>'.$res->subtotalPanier.'</td>
                        <td>
                          <div class="btn-group" role="group" aria-label="...">                         
                           <button title="augmenter" type="button" data-id="'.$res->idProd.'" data-stock="'.$res->stock.'" data-pu="'.$res->pu.'" data-designation="'.$res->designation.'" data-toggle="modal" data-target="#editPlus" class="btn btn-info btn-xs editPlus"><i class="glyphicon glyphicon-plus"></i></button>
                           <button title="diminuer" type="button" data-id="'.$res->idProd.'" data-stock="'.$res->stock.'" data-quantitepanier="'.$res->qtePanier.'"  data-pu="'.$res->pu.'" data-designation="'.$res->designation.'" data-toggle="modal" data-target="#editMoins" class="btn btn-warning btn-xs editMoins"><i class="glyphicon glyphicon-minus"></i></button>
                           <button title="annuler" type="button" data-id="'.$res->idProd.'" data-quantitepanier="'.$res->qtePanier.'" data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                          </div>   
                        </td>
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="7" style="padding-left:120px">Le panier est vide pour le moment</td></tr>';
	    }else{

	    }

	    echo $resultat; 
	}

	public function stockMoins($idProd , $quantite){
	 $this->db->query('UPDATE produit set stock= (stock -'.$quantite.') where idProd = '.$idProd.'' ) ;
	}

	public function stockPlus($idProd , $quantite){
	 $this->db->query('UPDATE produit set stock= (stock -'.$quantite.') where idProd = '.$idProd.'' ) ;
	}

	public function netPayer(){
		$netPayer = 0 ;
		$requete = $this->db->query('SELECT sum(subtotalPanier) as net from panier');
	    $res = $requete->row();
	     if (isset($res->net) AND $res->net != NULL){
	     	$netPayer = $res->net ; 
	     }
	     return $netPayer;
	}

	public function actualiseNet(){
		$requete = $this->db->query('SELECT sum(subtotalPanier) as net from panier');
	    $res = $requete->row();
	     if (isset($res->net) AND $res->net != NULL){
	     	echo '<tr><td colspan="4" style="">Ney à payer: '.$res->net.' Ariary</td></tr>'; 
	     	$rq = $this->db->query('SELECT * from client ORDER by idCli DESC');
	     	echo '<tr><td colspan="4" style=""><select class="form-control" style="" id="idClient">
	     		  <option  selected disabled="">--------Choisir le client puis valider achat----------</option>';
	     	foreach ($rq->result() as $res) {
	     		echo ' <option value ="'.$res->idCli.'">'.$res->nomCli.'</option>' ; 
	     	}
	     	echo '<select/><br> <button class="btn btn-info" id="enregistrerAchat" style="margin-left:130px" >Valider achat</button></td></tr>';

        }
	}

	public function actualiseBoutonVider(){
		$requete = $this->db->query('SELECT sum(subtotalPanier) as net from panier');
	    $res = $requete->row();
	     if (isset($res->net) AND $res->net != NULL){
	     	echo '<button class="pull-right" style="height:30px" id="viderPanier">Vider le panier</button>
                                 ' ; 
	     }
	}

	public function viderPanier(){
	 	$query = $this->db->query('SELECT * FROM panier');
		$res = $query->result();
		foreach ($res as $row) {
			$this->db->query('UPDATE produit set stock = (stock + '.$row->qtePanier.') WHERE idProd = '.$row->idProd.'  ' ) ;
		}
		$this->db->query('delete from panier');
		
	}

	public function additionner($idProd , $quantite, $pu){
	    $subtotal = $quantite * $pu ; 
		$this->db->query('UPDATE produit set stock= (stock - '.$quantite.') where idProd = '.$idProd.'' ) ;
	   	$this->db->query('UPDATE panier set qtePanier= (qtePanier + '.$quantite.') , subtotalPanier = (subtotalPanier + '.$subtotal.' ) where idProd = '.$idProd.'' ) ; 
	   
	}

	public function soustraire($idProd , $quantite, $pu){
	    $subtotal = $quantite * $pu ; 
		$this->db->query('UPDATE produit set stock= (stock + '.$quantite.') where idProd = '.$idProd.'' ) ;
	   	$this->db->query('UPDATE panier set qtePanier= (qtePanier - '.$quantite.') , subtotalPanier = (subtotalPanier - '.$subtotal.' ) where idProd = '.$idProd.'' ) ; 
	   
	}

	public function annuler($idProd , $quantite){
	 $this->db->query('UPDATE produit set stock= (stock + '.$quantite.') where idProd = '.$idProd.'' ) ;
	 $this->db->query('DELETE from panier where idProd='.$idProd ) ;
	
	}

	public function enregistrementAchat($idCli){
		$facture=0 ; 
		$queryFacture =  $this->db->query('SELECT max(factureAchat) as dernierFacture from achat');
		$res = $queryFacture->row();
	    if(isset ($res->dernierFacture) && $res->dernierFacture !=NULL){
	         $facture =  $res->dernierFacture + 1 ; 
	    }else{
			$facture = 1 ;  
	    }		 

		$query = $this->db->query('SELECT * FROM panier');
		$res = $query->result();
		foreach ($res as $row) {
			$data = array(
				'idCli' => $idCli ,
				'idProd' =>$row->idProd ,
		 		'qteAchat' =>$row->qtePanier ,
		 		'subtotalAchat' => $row->subtotalPanier , 
		 		'factureAchat' => $facture ,
	 		);
	 		$this->db->insert('achat' , $data);
		}

		if($facture > 0 ){//insertion dans table compte
			 $compte = 0 ; 
			 $querySomme =  $this->db->query('SELECT sum(subtotalPanier) as somme from panier');
		     $res = $querySomme->row();
	         if(isset ($res->somme) && $res->somme !=NULL){
	              $compte =  $res->somme ; 
	         }else{
		     	$compte = 0 ;  
	         }

			$action='ACHAT' ; 
			$operation ='PAYEMENT ACHAT';
			$factureAchat = 'achat N°'.$facture ; 
			$dateCompte = date('Y-m-d');
			$data = array(
				'action' =>$action,
		 		'dateCompte' =>$dateCompte ,
		 		'sommeEntre' => $compte, 
		 		'factureEntre' => $factureAchat ,
	 		);
	 		$this->db->insert('compte' , $data);
		}
		
	   $this->db->query('DELETE from panier' ) ;
	

	}

	public function actualiseAchat(){
		$resultat ='';
		$ligne=0 ;
		$query = $this->db->query('SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where achat.idCli = client.idCli group by achat.factureAchat ORDER by achat.factureAChat ASC ');
	 	foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '
					<tr>
                        <td>'.$res->dateAchat.'</td>
                        <td>'.$res->nomCli.'</td>
                        <td>'.$res->net.'</td>
                        <td>'.$res->factureAchat.'</td>
                        <td>
                        <button data-net="'.$res->net.'" data-facture="'.$res->factureAchat.'" data-date="'.$res->dateAchat.'"  data-nom="'.$res->nomCli.'" data-adresse="'.$res->adresseCli.'" data-telephone="'.$res->telephoneCli.'" class="btn btn-info detailler" data-toggle="modal" data-target="#details" >Details </button>
                                            <a class="btn btn-primary" href="'.base_url('Controller_achat/facture?nomCli='.$res->nomCli.'&telephoneCli='.$res->telephoneCli.'&adresseCli='.$res->adresseCli.'&dateAchat='.$res->dateAchat.'&factureAchat='.$res->factureAchat.'&net='.$res->net.'').'">Facture</a>
                                        </td>  
                                        </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="5" style="padding-left:450px">Aucun Enregistrement </td></tr>';
	    }

	    echo $resultat; 
	}
    

    public function search($saisi){
    	$req = 'SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where  (client.nomCli like "%'.$saisi.'%" 0R achat.dateAchat like "%'.$saisi.'%" )  AND achat.idCli = client.idCli  group by achat.factureAchat'; 
		$resultat ='';
		$ligne=0 ;
		$query = $this->db->query('SELECT client.* , achat.* , sum(subtotalAchat) as net FROM achat , client where  (client.nomCli like "%'.$saisi.'%" OR achat.dateAchat like "%'.$saisi.'%" OR achat.factureAchat like "%'.$saisi.'%" )  AND achat.idCli = client.idCli  group by achat.factureAchat ORDER by achat.factureAChat ASC ');
	 	foreach ($query->result() as $res) {
					$ligne++;
					$resultat .= '
					<tr>
                        <td>'.$res->dateAchat.'</td>
                        <td>'.$res->nomCli.'</td>
                        <td>'.$res->net.'</td>
                        <td>'.$res->factureAchat.'</td>
                        <td>
                          <button data-net="'.$res->net.'" data-facture="'.$res->factureAchat.'" data-date="'.$res->dateAchat.'"  data-nom="'.$res->nomCli.'" data-adresse="'.$res->adresseCli.'" data-telephone="'.$res->telephoneCli.'" class="btn btn-info detailler" data-toggle="modal" data-target="#details" >Details </button>
                          <a class="btn btn-primary" href="'.base_url('Controller_achat/facture?nomCli='.$res->nomCli.'&telephoneCli='.$res->telephoneCli.'&adresseCli='.$res->adresseCli.'&dateAchat='.$res->dateAchat.'&factureAchat='.$res->factureAchat.'&net='.$res->net.'').'">Facture</a>
                        </td>  
                                        
                    </tr> ';
				
	    }
	    if($ligne==0){
		   $resultat .= '<tr><td colspan="5" style="padding-left:450px">Aucun Enregistrement </td></tr>';
	    }

	    echo $resultat; 
	}

	public function details($id){
		$resultat ='';
		$query = $this->db->query('SELECT * FROM achat , produit , client where achat.idProd = produit.idProd AND achat.idCli = client.idCli AND achat.factureAchat ='.$id.' ');
	 	foreach ($query->result() as $res) {
					$resultat .= '
					<tr>
                        <td>'.$res->designation.'</td>
                        <td>'.$res->qteAchat.'</td>
                        <td>'.$res->subtotalAchat.'</td>
                    </tr> ';
				
	    }
	    
	    echo $resultat; 
	}

	public function listefact($id){
		$query = $this->db->query('SELECT * FROM achat , produit , client where achat.idProd = produit.idProd AND achat.idCli = client.idCli AND achat.factureAchat ='.$id.' ');
	 	return $query->result();
	}



}