<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_acceuil extends CI_Controller {

	public function index()
	{
		if( $this->session->userdata('username') !=""){
			$data['page']='acceuil';
			$this->load->model('Model_acceuil');
			$data['nombreClient']=$this->Model_acceuil->nombreClient();
			$data['nombreProduit']=$this->Model_acceuil->nombreProduit();
			$data['nombreAttente']=$this->Model_acceuil->nombreCommandeAttente();
			$data['livraison']=$this->Model_acceuil->nombreLivraison();
			$data['commandeJour']=$this->Model_acceuil->commandeJour();
			$data['achatJour']=$this->Model_acceuil->achatJour();

			$annee = date('Y');
			$janvier = '01'; $fevrier='02' ; $mars='03' ; $avril='04' ; $mais='05' ; $juin='06' ;
			$juillet = '07'; $aout='08' ; $septembre='09' ; $octobre='10' ; $novembre='11' ; $decembre='12' ;

			$data['entrerJanv']=$this->Model_acceuil->entrer($annee , $janvier);
			$data['sortirJanv']=$this->Model_acceuil->sortir($annee , $janvier);

			$data['entrerFev']=$this->Model_acceuil->entrer($annee , $fevrier);
			$data['sortirFev']=$this->Model_acceuil->sortir($annee , $fevrier);

			$data['entrerMars']=$this->Model_acceuil->entrer($annee , $mars);
			$data['sortirMars']=$this->Model_acceuil->sortir($annee , $mars);

			$data['entrerAvr']=$this->Model_acceuil->entrer($annee , $avril);
			$data['sortirAvr']=$this->Model_acceuil->sortir($annee , $avril);

			$data['entrerMais']=$this->Model_acceuil->entrer($annee , $mais);
			$data['sortirMais']=$this->Model_acceuil->sortir($annee , $mais);

			$data['entrerJuin']=$this->Model_acceuil->entrer($annee , $juin);
			$data['sortirJuin']=$this->Model_acceuil->sortir($annee , $juin);

			$data['entrerJuil']=$this->Model_acceuil->entrer($annee , $juillet);
			$data['sortirJuil']=$this->Model_acceuil->sortir($annee , $juillet);

			$data['entrerAout']=$this->Model_acceuil->entrer($annee , $aout);
			$data['sortirAout']=$this->Model_acceuil->sortir($annee , $aout);

			$data['entrerSept']=$this->Model_acceuil->entrer($annee , $septembre);
			$data['sortirSept']=$this->Model_acceuil->sortir($annee , $septembre);

			$data['entrerOct']=$this->Model_acceuil->entrer($annee , $octobre);
			$data['sortirOct']=$this->Model_acceuil->sortir($annee , $octobre);

			$data['entrerNov']=$this->Model_acceuil->entrer($annee , $novembre);
			$data['sortirNov']=$this->Model_acceuil->sortir($annee , $novembre);

			$data['entrerDec']=$this->Model_acceuil->entrer($annee , $decembre);
			$data['sortirDec']=$this->Model_acceuil->sortir($annee , $decembre);

			$this->load->view('view_acceuil' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	
}
