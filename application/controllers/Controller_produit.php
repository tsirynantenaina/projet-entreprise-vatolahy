<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_produit extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='produit';
			$this->load->model('Model_produit');
			$data['listeproduit']=$this->Model_produit->listeproduit();
			$data['listeReap']=$this->Model_produit->listeReap();
			$this->load->view('view_produit' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function inserer(){
			$designation=$this->input->post('designation'); 
			$stock=$this->input->post('stock'); 
			$pu=$this->input->post('pu') ;
			$this->load->model('Model_produit');
			$this->Model_produit->insertproduit( $designation , $stock , $pu);
			$this->Model_produit->actualiser();	
	}

	public function modifier(){
			$id=$this->input->post('id');
			$designation=$this->input->post('designation'); 
			$stock=$this->input->post('stock'); 
			$pu=$this->input->post('pu') ; ;
			$this->load->model('Model_produit');
			$this->Model_produit->editproduit($id , $designation , $stock , $pu);
			$this->Model_produit->actualiser();	
		
	}
	public function actualiserReap(){
			$this->load->model('Model_produit');;
			$this->Model_produit->actualiserReap();	
		
	}


	public function supprimer(){
			$id=$this->input->post('id');
			$this->load->model('Model_client');
			$this->Model_client->supprimeclient($id);
			$this->Model_client->actualiser();	
	}
	public function rechercher(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_produit');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_produit->search($saisi);
		}else{
			$this->Model_produit->actualiser();
		}
			

	}

	public function rechercherReap(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_produit');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_produit->searchReap($saisi);
		}else{
			$this->Model_produit->actualiserReap();
		}
			

	}


}