<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_export extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='export';
			$this->load->view('view_export' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}
}