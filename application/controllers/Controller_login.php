<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_login extends CI_Controller {

	public function index()
	{
		$this->load->view('view_login');
	}

	public function entrer(){
		$pseudo = $this->input->post('pseudo'); 
		$mdp = $this->input->post('mdp');
		$this->load->model('Model_login');
		$this->Model_login->validation($pseudo , $mdp)	; 					
	}

	public function deconnecter(){
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('type');
		redirect(base_url('Controller_login'));
	}
}
