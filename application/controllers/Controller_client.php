<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_client extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='client';
			$this->load->model('Model_client');
			$data['listeclient']=$this->Model_client->listeclient();
			$this->load->view('view_client' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function inserer(){
			$nom=$this->input->post('nom'); 
			$adresse=$this->input->post('adresse'); 
			$telephone=$this->input->post('telephone') ;
			$this->load->model('Model_client');
			$this->Model_client->insertclient( $nom , $adresse , $telephone);
			$this->Model_client->actualiser();	
	}

	public function modifier(){
			$id=$this->input->post('id');
			$nom=$this->input->post('nom'); 
			$adresse=$this->input->post('adresse'); 
			$telephone=$this->input->post('telephone') ;
			$this->load->model('Model_client');
			$this->Model_client->editclient($id ,  $nom , $adresse , $telephone);
			$this->Model_client->actualiser();	
	}


	public function supprimer(){
	    $id=$this->input->post('id');
	    $this->load->model('Model_client');
	    $this->Model_client->supprimeclient($id);
	    $this->Model_client->actualiser();	
	}

	public function rechercher(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_client');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_client->search($saisi);
		}else{
			$this->Model_client->actualiser();
		}
			

	}
	function action(){
	 $this->load->model('Model_client');
	 $this->load->library('excel');
	 $object = new PHPExcel();
	 $object->setActiveSheetIndex(0);
	 $table_columns = array('ID' , 'NOM CLIENT' , 'ADRESSE' , 'TELEPHONE' );
	 $column = 0 ; 
	 foreach ($table_columns as $field) {
	 	$object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
	 	$column ++ ; 
	 }

	 $client_data = $this->Model_client->listeclient();
	  $excel_row = 2 ; 
	  foreach ($client_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->idCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->nomCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->adresseCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->telephoneCli);
	 	
	 	$excel_row ++ ;

	 }
	 $object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');
	 header('Content-Type: application/vnd.ms-excel');
	 header('Content-Disposition: attachement;filename="ficheClient.xls"');
	 $object_writer->save('php://output');

	}



}