<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_achat extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='achat';
			$this->load->model('Model_achat');
			$data['listeproduit'] = $this->Model_achat->listeproduit();
			$data['listepanier'] = $this->Model_achat->listepanier();
			$data['netpayer']= $this->Model_achat->netPayer();
			$data['listeclient'] = $this->Model_achat->listeclient();
			$data['listeAchat'] = $this->Model_achat->listeAchat();
			$this->load->view('view_achat' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function panier(){
	   //inserer
	   $idProd=$this->input->post('idProd'); 
	   $quantite=$this->input->post('quantite'); 
	   $subtotal=$this->input->post('subtotal') ;
	   $this->load->model('Model_achat');
	   $this->Model_achat->insertpanier($idProd , $quantite , $subtotal);
	   
	   //diminuer stock produit
	   $this->Model_achat->stockMoins($idProd , $quantite);

	}

	public function actualisationProduit(){
		 $this->load->model('Model_achat');
	     $this->Model_achat->actualiseproduit();
	
	}
	public function actualisationPanier(){
		$this->load->model('Model_achat');
	    $this->Model_achat->actualisepanier();
	}

	public function actualisationNet(){
		$this->load->model('Model_achat');
	    $this->Model_achat->actualiseNet();
	}
	public function actualisationBoutonVider(){
		$this->load->model('Model_achat');
	    $this->Model_achat->actualiseBoutonVider();
	}

	public function vider(){
		$this->load->model('Model_achat');
	    $this->Model_achat->viderPanier();
	}

	public function addition(){
		$idProd=$this->input->post('idProd'); 
	    $quantite=$this->input->post('quantite'); 
	    $pu=$this->input->post('pu') ;
		$this->load->model('Model_achat');
	    $this->Model_achat->additionner( $idProd , $quantite , $pu);
	}

	public function soustraction(){
		$idProd=$this->input->post('idProd'); 
	    $quantite=$this->input->post('quantite'); 
	    $pu=$this->input->post('pu') ;
		$this->load->model('Model_achat');
	    $this->Model_achat->soustraire( $idProd , $quantite , $pu);
	}

	public function annuler(){
		$idProd=$this->input->post('idProd'); 
	    $quantite=$this->input->post('quantite'); 
		$this->load->model('Model_achat');
	    $this->Model_achat->annuler( $idProd , $quantite);
	}

	public function enregister(){
		$idCli=$this->input->post('idCli'); 
		$this->load->model('Model_achat');
	    $this->Model_achat->enregistrementAchat( $idCli);
	    $this->Model_achat->actualiseAchat();

	}

	public function rechercher(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_achat');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_achat->search($saisi);
		}else{
			$this->Model_achat->actualiseAchat();
		}
	}

	public function details(){
		$id=$this->input->post('id');
		$this->load->model('Model_achat');
	    $this->Model_achat->details($id);
		
	}

	public function facture(){
		$id=$this->input->get('factureAchat');
		$data['nomCli'] = $this->input->get('nomCli');
		$data['telephoneCli'] = $this->input->get('telephoneCli');
		$data['adresseCli'] = $this->input->get('adresseCli');
		$data['dateAchat'] = $this->input->get('dateAchat');
		$data['net'] = $this->input->get('net');
		$data['facture'] = $id ;
		$this->load->model('Model_achat');
	    $data['listeFact']=$this->Model_achat->listefact($id);
		$this->load->view('factureAchat' , $data);
		
	}

	

}