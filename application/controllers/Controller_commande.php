<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_commande extends CI_Controller {
	public function index(){	
		if($this->session->userdata('username') !=""){
			$data['page']='commande';
			$this->load->model('Model_commande');
			$data['listeclient']=$this->Model_commande->listeclient();
			$data['listeCommande']=$this->Model_commande->listeCommande(' client.idCli = commande.idCli AND commande.emargement = "EN ATTENTE"');
			$data['commandePayer']=$this->Model_commande->listeCommande(' client.idCli = commande.idCli AND commande.emargement = "LIVRE-PAYE"');
			$data['commandeNonPayer']=$this->Model_commande->listeCommande(' client.idCli = commande.idCli AND commande.emargement = "LIVRE-NON PAYE"');
			$data['commandeAnnuler']=$this->Model_commande->listeCommande(' client.idCli = commande.idCli AND commande.emargement = "DECOMMANDE"');
			$data['tousCommande']=$this->Model_commande->listeCommande(' client.idCli = commande.idCli ');

			$this->load->view('view_commande' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}


	public function inserer(){
	    $idCli=$this->input->post('idCli'); 
	    $commande=$this->input->post('commande'); 
	    $avance=$this->input->post('avance') ;
	    $montant=$this->input->post('montant') ;
	    $reste=$this->input->post('reste') ;
	    $emargement ='EN ATTENTE';
	    $dateLiv=$this->input->post('dateLiv') ;
	    $this->load->model('Model_commande');
	    $this->Model_commande->insertcommande($idCli , $commande , $dateLiv , $montant , $avance , $reste , $emargement); 	
		$this->Model_commande->actualiseCommande(' client.idCli = commande.idCli AND commande.emargement = "EN ATTENTE"'); 	
		
	}

	public function getCli(){
		$idCli=$this->input->post('idCli');
		$this->load->model('Model_commande');
	    $this->Model_commande->getClient($idCli); 	
		
	}

	public function modifier(){
		$idCom=$this->input->post('idCom'); 
	    $idCli=$this->input->post('idCli'); 
	    $commande=$this->input->post('commande'); 
	    $avance=$this->input->post('avance') ;
	    $montant=$this->input->post('montant') ;
	    $reste=$this->input->post('reste') ;
	    $emargement ='en attente';
	    $dateLiv=$this->input->post('dateLiv') ;
	    $this->load->model('Model_commande');
	    $this->Model_commande->editCommande($idCom , $idCli , $commande , $dateLiv , $montant , $avance , $reste , $emargement); 	
		$this->Model_commande->actualiseCommande('client.idCli = commande.idCli AND commande.emargement = "EN ATTENTE"'); 	
		
	}

	public function livrer(){
		$idCom=$this->input->post('idCom'); 
		$mode=$this->input->post('mode'); 
		$avance=$this->input->post('avance'); 
		$reste=$this->input->post('reste');
		$montant=$this->input->post('montant');  
	    $this->load->model('Model_commande');
	    $this->Model_commande->livraison($idCom , $mode , $montant , $avance , $reste );
	    $this->Model_commande->actualiseCommande(' client.idCli = commande.idCli  AND commande.emargement = "EN ATTENTE"');

	}

	public function actualiserpl(){
		$this->load->model('Model_commande');
	    $this->Model_commande->actualisePayer('client.idCli = commande.idCli AND commande.emargement = "LIVRE-PAYE"');
	}

	public function nonpayer(){
		$this->load->model('Model_commande');
	    $this->Model_commande->actualiseNonPayer('client.idCli = commande.idCli  AND commande.emargement = "LIVRE-NON PAYE"');
	}

    public function modePayer(){
		echo '<option value="LIVRE-PAYE">livré et payé</option>
              <option value="LIVRE-NON PAYE">livré mais non payé</option>';
	}

	public function annuler(){
		$idCom=$this->input->post('idCom'); 
		$emargement = 'DECOMMANDE' ; 
		$this->load->model('Model_commande');
	    $this->Model_commande->annulation($idCom , $emargement);
	     $this->Model_commande->actualiseCommande('client.idCli = commande.idCli  AND commande.emargement = "EN ATTENTE"');
	}

	public function decommande(){
		$this->load->model('Model_commande');
	    $this->Model_commande->actualiseDecom('client.idCli = commande.idCli  AND commande.emargement = "DECOMMANDE"');
	}
	public function tous(){
		$this->load->model('Model_commande');
	    $this->Model_commande->actualiseTous('client.idCli = commande.idCli  ');
	}
	public function facture(){
		$this->load->model('Model_commande');
	    $this->Model_commande->actualiseFact('client.idCli = commande.idCli  ');
	}

	public function regler(){
		$idCom=$this->input->post('idCom'); 
		$net=$this->input->post('net'); 
		$reste=$this->input->post('reste');
		$emargement = 'LIVRE-PAYE' ; 
		$this->load->model('Model_commande');
	    $this->Model_commande->regulation($idCom , $emargement , $net , $reste);
	     $this->Model_commande->actualiseCommande('client.idCli = commande.idCli AND commande.emargement = "en attente"');
	}


	public function rechercherCom(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualiseCommande(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%") AND commande.emargement="EN ATTENTE" AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualiseCommande('client.idCli = commande.idCli AND commande.emargement = "EN ATTENTE"');
		}
	}

	public function rechercherPayer(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualisePayer(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%") AND commande.emargement="LIVRE-PAYE" AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualisePayer('client.idCli = commande.idCli AND commande.emargement = "LIVRE-PAYE"');
		}
	}

	public function rechercherNonPayer(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualiseNonPayer(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%") AND commande.emargement="LIVRE-NON PAYE" AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualiseNonPayer('client.idCli = commande.idCli AND commande.emargement = "LIVRE-NON PAYE"');
		}
	}

	public function rechercherDecom(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualiseDecom(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%") AND commande.emargement="DECOMMANDE" AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualiseDecom('client.idCli = commande.idCli AND commande.emargement = "DECOMMANDE"');
		}
	}

	public function rechercherTous(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualiseTous(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%" OR commande.emargement like "%'.$saisi.'%") AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualiseTous('client.idCli = commande.idCli ');
		}
	}

	public function rechercherFact(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_commande');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_commande->actualiseFact(' (commande.dateCom like "%'.$saisi.'%" OR commande.commande like "%'.$saisi.'%"  OR client.nomCli like "%'.$saisi.'%" OR commande.Net like "%'.$saisi.'%" OR commande.avance like "%'.$saisi.'%" OR commande.reste like "%'.$saisi.'%" OR commande.idCom like "%'.$saisi.'%" OR commande.dateLivraison like "%'.$saisi.'%" OR commande.emargement like "%'.$saisi.'%") AND client.idCli=commande.idCli'); 
		}else{
			$this->Model_commande->actualiseFact('client.idCli = commande.idCli ');
		}
	}

	public function factureCom(){
		$data['nomCli'] = $this->input->get('nomCli');
		$data['telephoneCli'] = $this->input->get('telephoneCli');
		$data['adresseCli'] = $this->input->get('adresseCli');
		$data['dateCom'] = $this->input->get('dateCom');
		$data['Net'] = $this->input->get('Net');
		$data['avance'] = $this->input->get('avance');
		$data['idCom'] = $this->input->get('idCom');
		$data['emargement'] = $this->input->get('emargement');
		$data['dateLivraison'] = $this->input->get('dateLivraison');
		$data['commande'] = $this->input->get('commande');
		$this->load->model('Model_commande');
		$this->load->view('factureCommande' , $data);
		
	}
}