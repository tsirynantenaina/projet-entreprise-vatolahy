<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_depense extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='depense';
			$this->load->model('Model_depense');
			$data['listeDepense']=$this->Model_depense->listeDepense();
			$this->load->view('view_depense' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function inserer(){
			$action=$this->input->post('action'); 
			$operation=$this->input->post('operation'); 
			$somme=$this->input->post('somme') ;
			$this->load->model('Model_depense');
			$dateCompte = date('Y-m-d');
			$this->Model_depense->insertDepense($dateCompte , $action, $operation , $somme);
			$this->Model_depense->actualiser();	
	}

	public function modifier(){
		    $id=$this->input->post('id');
			$operation=$this->input->post('operation'); 
			$somme=$this->input->post('somme') ;
			$this->load->model('Model_depense');
			$this->Model_depense->editDepense($id , $operation , $somme);
			$this->Model_depense->actualiser();	
	}

	public function supprimer(){
	    $id=$this->input->post('id');
	    $this->load->model('Model_depense');
	    $this->Model_depense->supprimeDepense($id);
	    $this->Model_depense->actualiser();	
	}

	public function rechercher(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_depense');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_depense->search($saisi);
		}else{
			$this->Model_depense->actualiser();
		}
	}
}