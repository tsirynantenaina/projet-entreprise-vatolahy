<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_recouvrement extends CI_Controller {
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$anneeActuel = date('Y');
			$data['page']='recouvrement';
			$this->load->model('Model_recouvrement');
			$data['listeAchat'] = $this->Model_recouvrement->listeAchat($anneeActuel);
			$data['tousCommande']=$this->Model_recouvrement->listeCommande(' client.idCli = commande.idCli AND dateCom like "'.$anneeActuel.'%" ');
		    $data['totalAchat'] = $this->Model_recouvrement->totalAchat($anneeActuel); 
			$data['totalMontant'] = $this->Model_recouvrement->totalMontant($anneeActuel);
			$data['totalAvance'] = $this->Model_recouvrement->totalAvance($anneeActuel);
			$data['totalReste'] = $this->Model_recouvrement->totalReste($anneeActuel);
			$data['sommeRecu'] = $this->Model_recouvrement->sommeRecu($anneeActuel);
			$this->load->view('view_recouvrement' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function rechercherAchat(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_recouvrement->searchAchat($saisi);
	    }
	}
	public function rechercherCommande(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_recouvrement->searchCom('client.idCli = commande.idCli AND dateCom like "'.$saisi.'%"');
	    }
	}

	public function totalAchat(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        echo $this->Model_recouvrement->totalAchat($saisi);
	    }
	}

	public function totalMontant(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        echo $this->Model_recouvrement->totalMontant($saisi);
	    }
	}

	public function totalReste(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        echo $this->Model_recouvrement->totalReste($saisi);
	    }
	}

	public function totalAvance(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        echo $this->Model_recouvrement->totalAvance($saisi);
	    }
	}

	function action(){
	 $this->load->model('Model_recouvrement');
	 $this->load->library('excel');
	 $annee=$this->input->post('anneeRec');
	 $object = new PHPExcel();
	 $object->setActiveSheetIndex(0);
	 $table_columns = array('N°' , 'NOM CLIENT'  , 'TELEPHONE', 'ADRESSE' ,'N° fACT' , 'Du date' , 'Montant(FMG)' );
	 $column = 0 ; 
	 foreach ($table_columns as $field) {
	 	$object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
	 	$column ++ ; 
	 }

	  $achat_data = $this->Model_recouvrement->listeAchat($annee);
	  $excel_row = 3 ; 
	  $object->getActiveSheet()->setCellValueByColumnAndRow(0,2, 'ACHAT');
	  foreach ($achat_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$excel_row-2);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->nomCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->telephoneCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->adresseCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,'Achat N°'.$row->factureAchat);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$row->dateAchat);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$row->net);
	 	$excel_row ++ ;

	 }

	 $achat_total = $this->Model_recouvrement->totalAchat($annee);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row, 'TOTAL');
	 $object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row, $achat_total);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row+1, 'COMMANDE');
	 

	 $excel_row2= $excel_row+2;
	 $table_columns_commande = array('N°' , 'NOM CLIENT'  , 'TELEPHONE', 'ADRESSE' ,'N° fACT' , 'Du date' , 'Montant(FMG)' , 'Avance(FMG)' , 'RESTE(FMG)' , 'EMARGEMENT','Date livraison', 'Totalité recu' );
	 $column2 = 0 ;
	 foreach ($table_columns_commande as $field2) {
	 	$object->getActiveSheet()->setCellValueByColumnAndRow($column2,$excel_row2,$field2);
	 	$column2 ++ ; 
	 }

      $commande_data=$this->Model_recouvrement->listeCommande(' client.idCli = commande.idCli AND dateCom like "'.$annee.'%" ');
	  $ligne = 0 ; 
	  foreach ($commande_data as $row) { $ligne++ ; 
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row2,$ligne);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row2,$row->nomCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row2,$row->telephoneCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row2,$row->adresseCli);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row2,'Facture commande N°'.$row->idCom);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row2,$row->dateCom);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row2,$row->Net);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row2,$row->avance);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row2,$row->reste);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row2,$row->emargement);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(10,$excel_row2,$row->jourLivrance);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(11,$excel_row2,$row->sommeRecu);
	 	$excel_row2 ++ ;
	 }


	 $totalMontant = $this->Model_recouvrement->totalMontant($annee);
     $totalAvance = $this->Model_recouvrement->totalAvance($annee);
     $totalReste = $this->Model_recouvrement->totalReste($annee);
     $sommeRecu = $this->Model_recouvrement->sommeRecu($annee);
     $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row2,'TOTAL');
     $object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row2,$totalMontant);
     $object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row2,$totalAvance);
     $object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row2,$totalReste);
     $object->getActiveSheet()->setCellValueByColumnAndRow(11,$excel_row2,$sommeRecu);

	 $object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');
	 header('Content-Type: application/vnd.ms-excel');
	 header('Content-Disposition: attachement;filename="recouvrement'.$annee.'.xls"');
	 $object_writer->save('php://output');



	}


	public function totalRecu(){
		$this->load->model('Model_recouvrement');
		$saisi=$this->input->post('saisi');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        echo $this->Model_recouvrement->sommeRecu($saisi);
	    }
	}

}