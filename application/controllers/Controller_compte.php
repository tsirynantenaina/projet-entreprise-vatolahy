<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_compte extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$annee = date('Y');
			$janvier = '01'; $fevrier = '02'; $mars= '03'; $avril= '04'; $mai = '05'; $juin = '06';  $juillet = '07'; $aout = '08'; $septembre = '09'; $octobre = '10'; $novembre = '11'; $decembre = '12';   
			$data['page']='compte';
			$this->load->model('Model_compte');
			$data['sommeEntre']=$this->Model_compte->sommeTotalEntre($annee);
			$data['sommeSortie']=$this->Model_compte->sommeTotalSortie($annee);

			//janvier
			$data['compteJanvier']=$this->Model_compte->compte($annee ,$janvier);
			$data['entrerJanvier']=$this->Model_compte->sommeEntre($annee ,$janvier);
			$data['sortirJanvier']=$this->Model_compte->sommeSortie($annee ,$janvier);

			//Fevrier
			$data['comptefevirer']=$this->Model_compte->compte($annee ,$fevrier);
			$data['entrerfevirer']=$this->Model_compte->sommeEntre($annee ,$fevrier);
			$data['sortirfevirer']=$this->Model_compte->sommeSortie($annee ,$fevrier);

			//Mars
			$data['compteMars']=$this->Model_compte->compte($annee ,$mars);
			$data['entrerMars']=$this->Model_compte->sommeEntre($annee ,$mars);
			$data['sortirMars']=$this->Model_compte->sommeSortie($annee ,$mars);

			//Avril
			$data['compteAvril']=$this->Model_compte->compte($annee ,$avril);
			$data['entrerAvril']=$this->Model_compte->sommeEntre($annee ,$avril);
			$data['sortirAvril']=$this->Model_compte->sommeSortie($annee ,$avril);

			//Mai
			$data['compteMai']=$this->Model_compte->compte($annee ,$mai);
			$data['entrerMai']=$this->Model_compte->sommeEntre($annee ,$mai);
			$data['sortirMai']=$this->Model_compte->sommeSortie($annee ,$mai);

			//Juin
			$data['compteJuin']=$this->Model_compte->compte($annee ,$juin);
			$data['entrerJuin']=$this->Model_compte->sommeEntre($annee ,$juin);
			$data['sortirJuin']=$this->Model_compte->sommeSortie($annee ,$juin);

			//Juillet
			$data['compteJuillet']=$this->Model_compte->compte($annee ,$juillet);
			$data['entrerJuillet']=$this->Model_compte->sommeEntre($annee ,$juillet);
			$data['sortirJuillet']=$this->Model_compte->sommeSortie($annee ,$juillet);

			//auot
			$data['compteAout']=$this->Model_compte->compte($annee ,$aout);
			$data['entrerAout']=$this->Model_compte->sommeEntre($annee ,$aout);
			$data['sortirAout']=$this->Model_compte->sommeSortie($annee ,$aout);


            //septembre
			$data['compteSeptembre']=$this->Model_compte->compte($annee ,$septembre);
			$data['entrerSeptembre']=$this->Model_compte->sommeEntre($annee ,$septembre);
			$data['sortirSeptembre']=$this->Model_compte->sommeSortie($annee ,$septembre);

			//octobre
			$data['compteOctobre']=$this->Model_compte->compte($annee ,$octobre);
			$data['entrerOctobre']=$this->Model_compte->sommeEntre($annee ,$octobre);
			$data['sortirOctobre']=$this->Model_compte->sommeSortie($annee ,$octobre);

			//novembre
			$data['compteNovembre']=$this->Model_compte->compte($annee ,$novembre);
			$data['entrerNovembre']=$this->Model_compte->sommeEntre($annee ,$novembre);
			$data['sortirNovembre']=$this->Model_compte->sommeSortie($annee ,$novembre);


			//Decembre
			$data['compteDecembre']=$this->Model_compte->compte($annee ,$decembre);
			$data['entrerDecembre']=$this->Model_compte->sommeEntre($annee ,$decembre);
			$data['sortirDecembre']=$this->Model_compte->sommeSortie($annee ,$decembre);


			$this->load->view('view_compte' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	function action(){
	$annee = $this->input->post('anneeRec');
	$janvier = '01'; $fevrier = '02'; $mars= '03'; $avril= '04'; $mais = '05'; $juin = '06';  $juillet = '07'; $aout = '08'; $septembre = '09'; $octobre = '10'; $novembre = '11'; $decembre = '12';   
			
	 $this->load->model('Model_compte');
	 $this->load->library('excel');
	 $object = new PHPExcel();
	 $object->setActiveSheetIndex(0);
	 $table_columns = array('Date' , 'Facture' , 'ENTREE' , 'SORTIE','OPERATION');
	 $column = 0 ; 
	 foreach ($table_columns as $field) {
	 	$object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
	 	$column ++ ; 
	 }

//janvier
	 $janvier_data = $this->Model_compte->compte($annee , $janvier);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,2,'MOIS JANVIER ');
	  $excel_row = 3 ; 
	  foreach ($janvier_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerJanv = $this->Model_compte->sommeEntre($annee , $janvier);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerJanv );

	   $sortirJanv = $this->Model_compte->sommeSortie($annee , $janvier);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirJanv );

//Fevrier
	 $fevrier_data = $this->Model_compte->compte($annee , $fevrier);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS FEVRIER ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($fevrier_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerFev = $this->Model_compte->sommeEntre($annee , $fevrier);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerFev );

	   $sortirFev = $this->Model_compte->sommeSortie($annee , $fevrier);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirFev );


//Fevrier
	$mars_data = $this->Model_compte->compte($annee ,$mars);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS MARS ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($mars_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerMars = $this->Model_compte->sommeEntre($annee ,$mars);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerMars );

	   $sortirMars = $this->Model_compte->sommeSortie($annee ,$mars);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirMars );

//AVRIL
	$avril_data = $this->Model_compte->compte($annee ,$avril);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS AVRIL ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($avril_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerAvr = $this->Model_compte->sommeEntre($annee ,$avril);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerAvr );

	   $sortirAvr = $this->Model_compte->sommeSortie($annee ,$avril);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirAvr );

//Mai
	$mais_data = $this->Model_compte->compte($annee ,$mais);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS MAI ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($mais_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerMais = $this->Model_compte->sommeEntre($annee ,$mais);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerMais );

	   $sortirMais = $this->Model_compte->sommeSortie($annee ,$mais);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirMais );


//Mai
	$juin_data = $this->Model_compte->compte($annee ,$juin);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS JUIN');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($juin_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerJuin = $this->Model_compte->sommeEntre($annee ,$juin);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerJuin );

	   $sortirJuin = $this->Model_compte->sommeSortie($annee ,$juin);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirJuin );



//Mai
	$juillet_data = $this->Model_compte->compte($annee ,$juillet);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS JUILLET ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($juillet_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerJuillet = $this->Model_compte->sommeEntre($annee ,$juillet);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerJuillet );

	   $sortirJuillet = $this->Model_compte->sommeSortie($annee ,$juillet);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirJuillet );


//Aout
	$aout_data = $this->Model_compte->compte($annee ,$aout);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS AOUT ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($aout_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerAout = $this->Model_compte->sommeEntre($annee ,$aout);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerAout );

	   $sortirAout = $this->Model_compte->sommeSortie($annee ,$aout);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirAout );



 //Septembre
	$septembre_data = $this->Model_compte->compte($annee ,$septembre);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS SEPTEMBRE ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($septembre_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerSept = $this->Model_compte->sommeEntre($annee ,$septembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerSept );

	   $sortirSept = $this->Model_compte->sommeSortie($annee ,$septembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirSept );


//Octobre
	$octobre_data = $this->Model_compte->compte($annee ,$octobre);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS OCTOBRE ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($octobre_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerOct = $this->Model_compte->sommeEntre($annee ,$octobre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerOct );

	   $sortirOct = $this->Model_compte->sommeSortie($annee ,$octobre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirOct );


//Novembre
	$novembre_data = $this->Model_compte->compte($annee ,$novembre);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS NOVEMBRE ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($novembre_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerNov = $this->Model_compte->sommeEntre($annee ,$novembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerNov );

	   $sortirNov = $this->Model_compte->sommeSortie($annee ,$novembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirNov );

//Decembre
	$decembre_data = $this->Model_compte->compte($annee ,$decembre);
	 $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'MOIS NOVEMBRE ');
	  $excel_row = $excel_row + 2 ; 
	  foreach ($decembre_data as $row) {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->dateCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->factureEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->sommeEntre);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->sommeCompte);
	 	$object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->operation);
	 	$excel_row ++ ;
	  }
	   $entrerDec = $this->Model_compte->sommeEntre($annee ,$decembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$entrerDec );

	   $sortirDec = $this->Model_compte->sommeSortie($annee ,$decembre);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$sortirDec );



     $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row + 1,'TOTAL GENERAL ');
	 $entrertotal = $this->Model_compte->sommeTotalEntre($annee );
	 $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row+2,$entrertotal );
	 $sortirtotal = $this->Model_compte->sommeTotalSortie($annee );
	 $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row+2,$sortirtotal );























	 $object_writer = PHPExcel_IOFactory::createWriter($object,'Excel5');
	 header('Content-Type: application/vnd.ms-excel');
	 header('Content-Disposition: attachement;filename="comptable'.$annee.'.xls"');
	 $object_writer->save('php://output');



	}
}