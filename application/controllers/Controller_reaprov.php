<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_reaprov extends CI_Controller {
	
	public function index(){	
		if( $this->session->userdata('username') !=""){
			$data['page']='reaprov';
			$this->load->model('Model_reaprov');
			$data['listeproduit']=$this->Model_reaprov->listeproduit();
			$this->load->view('view_reaprov' , $data);
		}else{
			redirect(base_url('Controller_login'));
		}
	}

	public function reaprov(){
		    $id=$this->input->post('id'); 
			$qte=$this->input->post('qteReaprov'); 
			$this->load->model('Model_reaprov');
			$this->Model_reaprov->reaprov($id , $qte);
			$this->Model_reaprov->actualiser();
	}

	public function rechercher(){
		$saisi=$this->input->post('saisi');
		$this->load->model('Model_reaprov');
		if(isset($saisi) AND $saisi !=NULL AND $saisi !=""){
	        $this->Model_reaprov->search($saisi);
		}else{
			$this->Model_reaprov->actualiser();
		}
			

	}
	


}