<?php include('layout/header.php') ; ?>
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
     <script>function printContent(el){
      var restorepage=document.body.innerHTML;
      var printContent=document.getElementById(el).innerHTML;
      document.body.innerHTML=printContent;
      window.print();
      document.body.innerHTML=restorepage;
    }
  </script>
  <style type="text/css">

  </style>
</head>
<body onload="" style="background-color : white">
<div class="wrapper">
<div style="width:1200px ; margin-left:60px ; margin-top:10px" id="imprimer">
   <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="  invoice-header">
                          <h5><i class=""></i> Facture Atelier Vatolahy</h5>
                            </div>

                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          De
                          <address>
                                          <strong>Atelier Vatolahy</strong>
                                          <br>Fianarantsoa
                                          <br>Tel: 034 25 771 00
                                          <br>Region Haute Matsiatra
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Vers
                          <address>
                                          <strong><?php  echo $nomCli; ?></strong>
                                          <br><?php  echo $adresseCli; ?></strong>
                                          <br><?php  echo $telephoneCli; ?></strong>
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <h5  style="">Aujourd'hui <?php echo date('d-m-Y'); ?></h5>
                          <br>
                           <b>facture achat N° : <?php echo $facture; ?> </b> 
                          
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="  table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Produit</th>
                                <th>Quantité acheteé</th>
                                <th>Subtotal</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                              <?php if(isset ($listeFact) AND $listeFact!=NULL){
                                  foreach ($listeFact as $res) {
                                     ?>
                                      <tr>
                                        <td> <?php echo $res->designation ; ?></td>
                                         <td> <?php echo $res->qteAchat ; ?></td>
                                         <td> <?php echo $res->subtotalAchat ; ?></td>
                                      </tr>
                                      <?php
                                  }

                                } ?>
                              
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-md-6">
                          
                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                           Toutes transactions effectués au sein de l'atelier est muni de ce presnet fature en tant que pieces justificatifs.
                           Ainsi ce facture renferme l'information de l'achat, de produit et du client.
                          </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                          <p class="lead">Achat effectué le <?php  echo $dateAchat ; ?></strong></p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">THT:</th>
                                  <td><?php echo $net; ?></td>
                                </tr>
                                <tr>
                                  <th>TVA (21%)</th>
                                  <td> <?php  $tva = ($net * 21) / 100 ; echo $tva ; ?> </td>
                                </tr>
                                <tr>
                                  <th>TTC:</th>
                                  <td><?php echo $net+$tva ; ?> </td>
                                </tr>
                                
                               
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                    </section>
                  </div>
                   </div>

                  <div class="pull-right" style=" margin-right:100px">
                     <button class="btn btn-info" onclick="printContent('imprimer')" title="impression"> <i class="fa fa-print"></i> Imprimer</button>
                     <a class="btn btn-primary" href="<?php echo base_url('Controller_achat') ; ?>"> <i class=""></i> Retour</a>
                  </div>
<!-- ./wrapper -->
</body>
</html>
