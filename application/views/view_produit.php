
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertProduit">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Produit </h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div>
                      <button class="btn btn-primary nouveauCli" data-toggle="modal" data-target="#addNew" style="background-color:#43749c ; font-size:15px">Ajouter un nouveau produit</button><hr>
                  </div>
                  <div class="x_content">

                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Mes produits</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Produit besoin de reaprovisionnement</a>
                      </li>
                      <!--<li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                      </li>-->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="x_content">
                                <div>
                                 <input type="text" id="rechercheProd" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>
                            
                               <table id="" class="table table-striped table-bordered" style="width:100%"><thead>
                                  <th>ID</th>
                                  <th>Designation</th>
                                  <th>Stock</th>
                                  <th>pu (ariary)</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyProd">
                                    <?php if(isset($listeproduit) AND $listeproduit !=NULL){ 
                                        foreach ($listeproduit as $res) { ?>
                                            <tr>
                                                 <td><?php echo $res->idProd; ?></td>
                                                <td><?php echo $res->designation; ?></td>
                                                <td><?php echo $res->stock; ?></td>
                                                <td><?php echo $res->pu; ?></td>
                                                <td>
                                                  <div class="btn-group" role="group" aria-label="...">                         
                                                   <button type="button" data-id="<?php echo $res->idProd; ?>" data-designation="<?php echo $res->designation; ?>" data-stock="<?php echo $res->stock; ?>" data-pu="<?php echo $res->pu; ?>" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd"><i class="glyphicon glyphicon-pencil"></i></button>
                                                   <button type="button" data-id="<?php echo $res->idProd; ?>" data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                                                  </div>  
                                                </td>
                                            </tr>
                                         <?php }
                                       }else{
                                          echo '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
                                 
                                          } ; ?>  
                                 </tbody>
                               </table>
                          </div>    
                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="x_content"> 
                              <div>
                                 <input type="text" id="rechercheProdReap" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>
                            
                               <table id="" class="table table-striped table-bordered" style="width:100%">
                               <thead>
                                <tr>
                                  <th colspan="5">Produit stock inferieur à 10</th>
                                </tr>
                                <tr>
                                  <th>ID</th>
                                  <th>Designation</th>
                                  <th>Stock</th>
                                  <th>pu (ariary)</th>
                                  <th>Actions</th>
                                <tr>
                                </thead>
                                <tbody id="tbodyProdReap">
                                    <?php if(isset($listeReap) AND $listeReap !=NULL){ 
                                        foreach ($listeReap as $res) { ?>
                                            <tr>
                                                 <td><?php echo $res->idProd; ?></td>
                                                <td><?php echo $res->designation; ?></td>
                                                <td><?php echo $res->stock; ?></td>
                                                <td><?php echo $res->pu; ?></td>
                                                <td>
                                                    <a class="btn btn-primary" href="<?php echo base_url('Controller_reaprov');?>">Raprovisionner</a>  
                                                </td>
                                            </tr>
                                         <?php }
                                       }else{
                                          echo '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
                                 
                                          } ; ?>  
                                 </tbody>
                               </table>

                         </div>
                      </div>
                      
                    </div>
                  </div>

                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 
<!-- Add new modal -->
<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Ajout d'un nouveau produit</h4>
        <button type="button" class="close fermerAdd" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifAdd">
        </div>
        <p>Veuillez remplir ce formulaire pour ajouter un produit</p>

        <form id="usineForm" method="post"> 
          <div class="form-group">
            <i id="idesignation" style="color:red"></i>
            <input type="text" class="form-control" id="designation" placeholder="Designation" >
          </div>
          <div class="form-group">
            <i id="istock" style="color:red"></i>
            <input type="number" class="form-control" id="stock" placeholder="Stock" style="color:black">
          </div>
          <div class="form-group">
            <i id="ipu" style="color:red"></i>
            <input type="number" class="form-control" id="pu"  style="color:black" placeholder="Prix Unitaire">
          </div>          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary" id="saveProd" style="font-size:15px">Ajouter</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



 <!-- Edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edition d'un produit</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Modifier la valeur du champ pour editer un produit</p>
        <form id="editForm" method="post"> 
          <div id="notifEdit"></div>
          <div class="form-group">
            <input type="hidden" class="form-control" id="idProdEdit" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="idesignationProdEdit" style="color:red"></i>
            <input type="text" class="form-control" id="designationProdEdit" placeholder="Designation" >
          </div>
          <div class="form-group">
            <i id="istockProdEdit" style="color:red"></i>
            <input type="text" class="form-control" id="stockProdEdit" placeholder="Stock">
          </div>
          <div class="form-group">
            <i id="ipuProdEdit" style="color:red"></i>
            <input type="text" class="form-control" id="puProdEdit" placeholder="Prix unitaire">
          </div> 

      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateProd" style="font-size:15px">Modifier</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>

<!-- Delete modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Suppression</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreSuppr"></p>
        <input type="hidden" id="idProdDelete">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteProd"  data-dismiss="modal" style="font-size:15px">Supprimer</button>          
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('layout/footer.php') ; ?>
<?php include('ajax/produit/ajax_produit.php') ; ?>
<?php include('ajax/produit/controlChampProduit.php') ; ?>