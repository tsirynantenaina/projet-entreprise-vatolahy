
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertDepense">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Depense</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div>
                      <button class="btn btn-primary " data-toggle="modal" data-target="#addNew" style="background-color:#43749c ; font-size:15px">Ajouter un nouveau dépense</button><hr>
                  </div>
                  

                  <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active cliJour" id="home-tab" data-toggle="tab" href="#" role="tab" aria-controls="" aria-selected="true">Historiques de depenses</a>
                      </li>
          
                    </ul>
                    
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="x_content">
                               <div>
                                 <input type="text" id="rechercheDepense" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>
                               <table id="" class="table table-striped table-bordered" style="width:100%">
                                 <thead>
                                  <th>Date</th>
                                  <th>Operation</th>
                                  <th>Somme</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyDepense">
                                    <?php if (isset($listeDepense) AND $listeDepense !=NULL){
                                         foreach ($listeDepense as $res) { ?>
                                            <tr>
                                                 <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td>
                                                  <div class="btn-group" role="group" aria-label="...">                         
                                                   <button type="button" data-id="<?php echo $res->idCompte ;  ?>" data-operation="<?php echo $res->operation ; ?>" data-somme="<?php echo $res->sommeCompte ; ?>"  data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editDepense"><i class="fa fa-pencil"></i></button>
                                                   <button type="button" data-id="<?php echo $res->idCompte ; ?>" data-operation="<?php echo $res->operation ; ?>" data-somme="<?php echo $res->sommeCompte ; ?>" data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i></button>
                                                  </div>  
                                                </td>
                                            </tr>
                                         <?php } 
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement  de dépense</td></tr>';
                                    }
                                  ?>  

                                 </tbody>
                               </table>

                         </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 
<!-- Add new modal -->
<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Ajout d'un depense</h4>
        <button type="button" class="close fermerAdd" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifAdd">
        </div>
        <p>Veuillez remplir ce formulaire pour un nouveau depense</p>

        <form id="usineForm" method="post"> 
          <div class="form-group">
            <i id="ioperation" style="color:red"></i>
            <input type="text" class="form-control" id="operation" placeholder="Operation" >
          </div>
          <div class="form-group">
            <i id="isomme" style="color:red"></i>
            <input type="number" class="form-control" id="somme" placeholder="Somme" style="color:black">
          </div>         
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary" id="saveDepense" style="font-size:15px">Ajouter</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



 <!-- Edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edition d'un depense</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Modifier la valeur du champ pour editer un depense</p>
        <form id="editForm" method="post"> 
          <div id="notifEdit"></div>
          <div class="form-group">
            <input type="text" class="form-control" id="idDepense" >
          </div>
          <div class="form-group">
            <i id="ioperationEdit" style="color:red"></i>
            <input type="text" class="form-control" id="operationEdit" placeholder="Operation" >
          </div>
          <div class="form-group">
            <i id="isommeEdit" style="color:red"></i>
            <input type="number" class="form-control" id="sommeEdit" placeholder="somme">
          </div> 
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateDepense" style="font-size:15px">Modifier</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>

<!-- Delete modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Suppression</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreSuppr"></p>
        <input type="hidden" id="idDelete">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteDepense"  data-dismiss="modal" style="font-size:15px">Supprimer</button>          
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('layout/footer.php') ; ?>
<?php include('ajax/depense/controlChampDepense.php') ; ?>
<?php include('ajax/depense/ajax_depense.php') ; ?>
