
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertProduit">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Mes produits</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                 
                  

                  <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active cliJour" id="home-tab" data-toggle="tab" href="#" role="tab" aria-controls="" aria-selected="true">Liste Produits</a>
                      </li>
                    </ul>
                    
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="x_content"> 
                              <div>
                                 <input type="text" id="rechercheProd" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>
                            
                               <table id="" class="table table-striped table-bordered" style="width:100%"><thead>
                                  <th>ID</th>
                                  <th>Designation</th>
                                  <th>Stock</th>
                                  <th>pu(ariary)</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyProd">
                                    <?php if(isset($listeproduit) AND $listeproduit !=NULL){ 
                                        foreach ($listeproduit as $res) { ?>
                                            <tr>
                                                 <td><?php echo $res->idProd; ?></td>
                                                <td><?php echo $res->designation; ?></td>
                                                <td><?php echo $res->stock; ?></td>
                                                <td><?php echo $res->pu; ?></td>
                                                <td>
                                                  <div class="btn-group" role="group" aria-label="...">                         
                                                   <button type="button" data-id="<?php echo $res->idProd; ?>" data-designation="<?php echo $res->designation; ?>" data-stock="<?php echo $res->stock; ?>" data-pu="<?php echo $res->pu; ?>" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editProd">Reaprovisionner</button>
                                                  
                                                </td>
                                            </tr>
                                         <?php }
                                       }else{
                                          echo '<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement</td></tr>';
                                 
                                          } ; ?>  
                                 </tbody>
                               </table>

                         </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 




 <!-- Edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Reaprovisionnement</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Veuillez entrer la quantité de reaprovisionnement</p>
        <form id="editForm" method="post"> 
          <div id="notifEdit"></div>
          <div class="form-group">
            <input type="hidden" class="form-control" id="idProd" >
          </div>
          <div class="form-group">
            <div type="text" class="form-control" id="nomProd"></div>
          </div>
          <div class="form-group">
            <input type="number" id="qteReaprov" class="form-control" placeholder="Quantité de reaprovision">
          </div>
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="reaprovProd" style="font-size:15px">Valider</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>

<!-- Delete modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Suppression</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreSuppr"></p>
        <input type="hidden" id="idProdDelete">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteProd"  data-dismiss="modal" style="font-size:15px">Supprimer</button>          
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('layout/footer.php') ; ?>
<?php include('ajax/reaprov/ajax_reaprov.php') ; ?>