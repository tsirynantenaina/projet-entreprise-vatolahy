
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertClient">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Export Excel</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  

                  <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active cliJour" id="home-tab" data-toggle="tab" href="#" role="tab" aria-controls="" aria-selected="true">Liste Clients</a>
                      </li>
          
                    </ul>
                    
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                       <div class="col-14">
                        <div class="x_panel col-4">
                          <h2 style="">Exporter fiche client</h2>
                          <hr>
                          <form method="post" action="<?php echo base_url('Controller_client/action');?>"> 
                            <label>Année:</label>
                            <input type="number" value="<?php echo date('Y') ; ?>" id="" class="form-control"> <br>
                          <button class="btn btn-primary" type="submit" id="ficheCli" style="margin-left:120px">Exporter</button>
                           </form>
                         
                          </div>
                        <div class="x_panel col-4">
                        <h2 style="">Exporter un recouvrement</h2>
                          <hr>
                         <form method="post" action="<?php echo base_url('Controller_recouvrement/action');?>"> 
                            <label>Année:</label>
                            <input type="number" name="anneeRec" value="<?php echo date('Y') ; ?>" id="" class="form-control"> <br>
                          <button class="btn btn-primary" type="submit"  style="margin-left:120px">Exporter</button>
                           </form>
                        </div>
                        <div class="x_panel" style="width:342px">
                          <h2 style="">Exporter un extrait compatble</h2>
                          <hr>
                          <form method="post" action="<?php echo base_url('Controller_compte/action');?>"> 
                            <label>Année:</label>
                            <input type="number" name="anneeRec" value="<?php echo date('Y') ; ?>" id="" class="form-control"> <br>
                          <button class="btn btn-primary" type="submit" id="ficheCli" style="margin-left:120px">Exporter</button>
                           </form>
                        </div>
                      </div>
                         
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 


<?php include('layout/footer.php') ; ?>
<?php include('ajax/client/ajax_client.php') ; ?>