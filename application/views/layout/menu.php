
<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-rebel"></i> <span>Atelier Vatolahy</span></a>
            </div>

            <div class="clearfix"></div>
            

            <br/>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3 style="margin-left:-10px">General</h3>
                <ul class="nav side-menu">
                <li <?php if($visiter =='acceuil'){ echo 'class="active" ' ; } ?> >
                    <a  href="<?php echo base_url('Controller_acceuil')?>"><i class="fa fa-home"></i>Acceuil</a>
                  </li>
                  
                  <li class="<?php if($visiter =='client'){echo 'active' ; } ?>" >
                    <a href="<?php echo base_url('Controller_client')?>"><i class="fa fa-user active"></i>Client </a>
                  </li>
                  <li class="<?php if($visiter =='produit'){echo 'active' ; } ?>" >
                    <a href="<?php echo base_url('Controller_produit')?>"><i class="fa fa-book"></i>Produit </a>
                  </li>
                  <li class="<?php if($visiter =='achat'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_achat')?>"><i class="fa fa-money"></i>Achat</a>
                  </li>
                  <li class="<?php if($visiter =='commande'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_commande')?>"><i class="fa fa-table"></i>Commande</a>
                  </li>
                  <li class="<?php if($visiter =='depense'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_depense')?>"><i class="fa fa-external-link"></i>Depense</a>
                  </li>

                  <li class="<?php if($visiter =='recouvrement'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_recouvrement')?>"><i class="fa   fa-gg-circle"></i>Recouvrement</a>
                  </li>
                  <li class="<?php if($visiter =='compte'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_compte')?>"><i class="fa  fa-calculator"></i>Compte</a>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3 style="margin-left:-10px">AUTRES</h3>
                <ul class="nav side-menu">
                   <li class="<?php if($visiter =='export'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_export')?>"><i class="fa fa-file"></i>Export Excel</a>
                  </li>
                  <li class="<?php if($visiter =='reaprov'){echo 'active' ; } ?> ">
                    <a href="<?php echo base_url('Controller_reaprov')?>"><i class="fa  fa-plus"></i>Reaprovisionnement</a>
                  </li>
                  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <!--<a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="fa fa-laptop" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="fa fa-laptop" aria-hidden="true"></span>
              </a>
              <a data-toggle="fa tooltip" data-placement="top" title="Lock">
                <span class="fa fa-laptop" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="fa fa-laptop" aria-hidden="true"></span>
              </a>-->
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
