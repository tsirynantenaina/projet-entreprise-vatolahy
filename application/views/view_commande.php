
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertCommande">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Commande </h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div>
                      <button class="btn btn-primary nouveauCli" data-toggle="modal" data-target="#addNew" style="background-color:#43749c ; font-size:15px">Ajouter un nouveau commande</button><hr>
                  </div>
                  

                  <div class="x_content">

                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        Commande en attente <label></label></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">commande livré-payé <label></label></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Commande livré-non payé <label></label></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Commande annulée <label></label></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact3" role="tab" aria-controls="contact" aria-selected="false">Tous les commande <label></label></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact4" role="tab" aria-controls="contact" aria-selected="false">Facture<label></label></a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                               <div>
                                 <input type="text" id="rechercheCom" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>

                              <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>Date commande</th>
                                  <th>Client</th>
                                  <th>Commande</th>
                                  <th>à livrer le</th>
                                  <th>Montant </th>
                                  <th>Avance</th>
                                  <th>Reste </th>
                                  <th>Emargement</th>
                                  <th>facture</th>
                                  <th>Edition</th>
                                  <th>Livraison</th>
                                </thead>
                                <tbody id="tbodyCom">
                                  <?php if (isset($listeCommande) AND $listeCommande !=NULL ){
                                    foreach ($listeCommande as $res) {    ?>
                                    <tr> 
                                        <td><?php echo date('Y-m-d',strtotime($res->dateCom)) ?></td>
                                        <td><?php echo $res->nomCli; ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($res->dateLivraison)) ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td><?php echo $res->reste; ?></td>
                                        <td><?php echo $res->emargement; ?></td>
                                        <td><?php echo $res->idCom; ?></td>
                                        <td>
                                          <div class="btn-group" role="group" aria-label="...">                         
                                            <button data-idcom ="<?php echo $res->idCom; ?>" data-idcli ="<?php echo $res->idCli; ?>" data-commande ="<?php echo $res->commande; ?>" data-livraison ="<?php echo $res->dateLivraison; ?>" data-net ="<?php echo $res->Net; ?>" data-avance="<?php echo $res->avance; ?>" data-reste="<?php echo $res->reste; ?>" title="modifier" type="button"  data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs editCom" ><i class="fa fa-pencil"></i></button>
                                            <button data-nom ="<?php echo $res->nomCli; ?>" data-idcom="<?php echo $res->idCom; ?>" title="annuler commande" type="button"  data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="fa fa-close"></i></button>
                                           </div> 
                                        </td>
                                        <td>
                                        <button data-idcom="<?php echo $res->idCom; ?>" data-nom="<?php echo $res->nomCli; ?>" data-montant="<?php echo $res->Net; ?>" data-avance="<?php echo $res->avance; ?>" data-reste="<?php echo $res->reste; ?>" class="btn btn-primary livraison" title="liver commande" data-toggle="modal" data-target="#livrer">livré</button>
                                        </td>
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="11" style="padding-left:450px">Aucun commande en attente </td></tr>';
                                    }?>
                                 </tbody>
                               </table>
                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                          <div>
                             <input type="text" id="recherchePayer" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                          </div>

                          <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>Date livrance </th>
                                  <th>Commande</th>
                                  <th>Montant </th>
                                  <th>Emargement</th>
                                  <th>somme recu</th>
                                  <th>facture</th>
                                </thead>
                                <tbody id="tbodypl">
                                    <?php if (isset($commandePayer) AND $commandePayer !=NULL ){
                                    foreach ($commandePayer as $res) {    ?>
                                    <tr> 
                                        <td>livré à <?php echo $res->nomCli; ?> le <?php echo date('d-m-Y',strtotime($res->jourLivrance)) ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->emargement; ?></td>
                                        <td>
                                         <?php echo $res->sommeRecu; ?> 
                                        </td>
                                        <td>
                                        <?php echo $res->idCom ; ?> 
                                       </td>
                                    </tr>
                                  <?php }
                                }else{
                                  echo'<tr><td colspan="6" style="padding-left:450px">Aucune commande livré et payé </td></tr>';
                                }
                                ?>
                                  
                                </tbody>
                              </table>
                      </div>
                      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div>
                           <input type="text" id="rechercheNonPayer" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                        </div>
                        <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>Date livrance</th>
                                  <th>Commande</th>
                                  <th>Montant </th>
                                  <th>Déja payé</th>
                                  <th>Reste </th>
                                  <th>Emargement</th>
                                  <th>facture</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodypnl">
                                    <?php if (isset($commandeNonPayer) AND $commandeNonPayer !=NULL ){
                                    foreach ($commandeNonPayer as $res) {    ?>
                                    <tr> 
                                        <td>livrée à <?php echo $res->nomCli; ?> le <?php echo date('d-m-Y',strtotime($res->jourLivrance)) ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td><?php echo $res->reste; ?></td>
                                        <td><?php echo $res->emargement; ?></td>
                                        <td>
                                         <?php echo $res->idCom; ?>
                                        </td>
                                        <td>
                                           <button data-idcom="<?php echo $res->idCom; ?>" data-net="<?php echo $res->Net; ?>" data-reste="<?php echo $res->reste; ?>" data-nom="<?php echo $res->nomCli; ?>" class="btn btn-primary regler" title="totalité payement regler" data-toggle="modal" data-target="#regler">reglé
                                           </button>
                                       </td>
                                    </tr>
                                  <?php }
                                }else{
                                  echo'<tr><td colspan="10" style="padding-left:450px">Aucune commande livré mais non payé </td></tr>';
                                }
                                ?>
                                  
                                </tbody>
                              </table>
                      </div>
                      <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab">
                        <div>
                           <input type="text" id="rechercheDecom" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                        </div>
                        <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>Date</th>
                                  <th>Client</th>
                                  <th>Commande</th>
                                  <th>Date livraison</th>
                                  <th>Montant </th>
                                  <th>Avance</th>
                                  <th>Reste </th>
                                  <th>Emargement</th>
                                  <th>facture</th>
                                  <th>Action</th>
                                </thead>
                                <tbody id="tbodydcd">
                                  <?php if (isset($commandeAnnuler) AND $commandeAnnuler !=NULL ){
                                    foreach ($commandeAnnuler as $res) {    ?>
                                    <tr> 
                                        <td><?php echo date('d-m-Y',strtotime($res->dateCom)) ?></td>
                                        <td><?php echo $res->nomCli; ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($res->dateLivraison)) ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td><?php echo $res->reste; ?></td>
                                        <td><?php echo $res->emargement; ?></td>
                                        <td><?php echo $res->idCom; ?></td>
                                        <td>
                                          <button data-idcom="<?php echo $res->idCom; ?>" data-nom="<?php echo $res->nomCli; ?>" data-montant="<?php echo $res->Net; ?>" data-avance="<?php echo $res->avance; ?>" data-reste="<?php echo $res->reste; ?>" class="btn btn-primary livraison" title="liver commande" data-toggle="modal" data-target="#livrer">???</button>
                                        </td>
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="10" style="padding-left:450px">Aucune commande annulée </td></tr>';
                                    }?>
                                 </tbody>
                               </table>
                      </div>
                      <div class="tab-pane fade" id="contact3" role="tabpanel" aria-labelledby="contact-tab">
                          <div>
                             <input type="text" id="recherchetous" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                           </div>

                           <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>facture</th>
                                  <th>Date commande</th>
                                  <th>Client</th>
                                  <th>Commande</th>
                                  <th>à livrer le</th>
                                  <th>Montant </th>
                                  <th>Avance</th>
                                  <th>Reste </th>
                                  <th>Emargement</th>
                                  <th>date de livraison</th>
                                  <th>Somme recu</th>
                                </thead>
                                <tbody id="tbodytous">
                                  <?php if (isset($tousCommande) AND $tousCommande !=NULL ){
                                    foreach ($tousCommande as $res) {    ?>
                                    <tr><?php $payement =$res->emargement ;  ?>
                                        <td><?php echo $res->idCom; ?></td>
                                        <td><?php echo date('Y-m-d',strtotime($res->dateCom)) ?></td>
                                        <td><?php echo $res->nomCli; ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($res->dateLivraison)) ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?>><?php echo $res->reste; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?> >
                                            <?php echo $res->emargement; ?>
                                        </td> 
                                        <td><?php echo $res->jourLivrance; ?></td>  
                                        <td><?php echo $res->sommeRecu; ?></td>                        
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="11" style="padding-left:450px">Aucun commande </td></tr>';
                                    }?>
                                 </tbody>
                               </table>

                      </div>
                      <div class="tab-pane fade" id="contact4" role="tabpanel" aria-labelledby="contact-tab">
                          <div>
                             <input type="text" id="rechercheFact" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                           </div>

                           <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>facture</th>
                                  <th>Date commande</th>
                                  <th>Client</th>
                                  <th>Commande</th>
                                  <th>à livrer le</th>
                                  <th>Montant </th>
                                  <th>Avance</th>
                                  <th>Reste </th>
                                  <th>Emargement</th>
                                  <th>date de livraison</th>
                                  <th>Somme recu</th>
                                </thead>
                                <tbody id="tbodyFact">
                                  <?php if (isset($tousCommande) AND $tousCommande !=NULL ){
                                    foreach ($tousCommande as $res) {    ?>
                                    <tr><?php $payement =$res->emargement ;  ?>
                                        <td><?php echo $res->idCom; ?></td>
                                        <td><?php echo date('Y-m-d',strtotime($res->dateCom)) ?></td>
                                        <td><?php echo $res->nomCli; ?></td>
                                        <td><?php echo $res->commande ; ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($res->dateLivraison)) ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?>><?php echo $res->reste; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?> >
                                            <?php echo $res->emargement; ?>
                                        </td> 
                                        <td><?php echo $res->jourLivrance; ?></td>  
                                        <td><?php echo $res->sommeRecu; ?></td>     
                                        <td><a class="btn btn-primary" href="<?php echo base_url('Controller_commande/factureCom?nomCli='.$res->nomCli.'&telephoneCli='.$res->telephoneCli.'&adresseCli='.$res->adresseCli.'&dateCom='.$res->dateCom.'&idCom='.$res->idCom.'&Net='.$res->Net.'&avance='.$res->avance.'&commande='.$res->commande.'&emargement='.$res->emargement.'&dateLivraison='.$res->dateLivraison.'') ; ?>">Facture</a></td>                   
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="11" style="padding-left:450px">Aucun commande </td></tr>';
                                    }?>
                                 </tbody>
                               </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>
  </body> 
 
<!-- Add new modal -->
<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Bon de commande</h4><label id="testa"></label>
        <button type="button" class="close fermerAddCom" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifAddCom">
        </div>
        <p>Veuillez remplir ce formulaire pour ajouter un commande</p>

        <form id="usineForm" method="post"> 
          <div class="form-group">
            <select class="form-control" id="idClient">
              <option disabled selected>Veuillez selectionner le client</option>
              <?php foreach ($listeclient as $res) { ?>
                 <option value="<?php echo $res->idCli; ?>"><?php echo $res->nomCli; ?></option>
              <?php }?>
            </select>
          </div>
          <div class="form-group">
            <i id="icom" style="color:red"></i>
            <textarea  class="form-control" id="com" placeholder="Description du commande"></textarea>
          </div>
          <div class="form-group">
            <i id="idateLiv" style="color:red"></i>
            <input type="text" class="form-control" id="dateLiv" placeholder="Date de livraison exemple 01-02-2021">
          </div>
          <div class="form-group">
            <label>Montant commande:</label>
            <i id="inetCom" style="color:red"></i>
            <input type="number" class="form-control" style="color:black" id="netCom" value="0" placeholder="Prix commande (Ariary) ">
          </div>  
          <div class="form-group">
            <label>Avance:</label>
            <i id="iavance" style="color:red"></i>
            <input type="number" class="form-control" style="color:black" id="avance" value="0" placeholder="Avance">
          </div> 
          <div class="form-group">
            <label>Reste:</label>
            <i id="ireste" style="color:red"></i> 
            <input type="number" class="form-control" style="color:black" id="reste" value="0" placeholder="Reste">
          </div>        
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary fermeAddCom" id="saveCom" style="font-size:15px">Ajouter</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



<!-- edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Edition d'un nouveau commande</h4><label id="testa"></label>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifEditCom">
        </div>
        <p>Veuillez remplir ce formulaire pour modifier un commande</p>

        <form id="usineForm" method="post"> 
          <input type="hidden" id="idComEdit">
          <div class="form-group">
            <select class="form-control" id="idClientEdit">
             
            </select>
          </div>
          <div class="form-group">
            <i id="icomEdit" style="color:red"></i>
            <textarea  class="form-control" id="comEdit" placeholder="Description du commande"></textarea>
          </div>
          <div class="form-group">
            <i id="idateLivEdit" style="color:red"></i>
            <input type="text" class="form-control" id="dateLivEdit" placeholder="Date de livraison ( exemple 01-02-2021)">
          </div>
          <div class="form-group">
            <label>Montant commande:</label>
            <i id="inetComEdit" style="color:red"></i>
            <input type="number" class="form-control" style="color:black" id="netComEdit" value="0" placeholder="Prix commande (Ariary) ">
          </div>  
          <div class="form-group">
            <label>Avance:</label>
            <i id="iavanceEdit" style="color:red"></i>
            <input type="number" class="form-control" style="color:black" id="avanceEdit" value="0" placeholder="Avance">
          </div> 
          <div class="form-group">
            <label>Reste:</label>
            <i id="iresteEdit" style="color:red"></i> 
            <input type="number" class="form-control" style="color:black" id="resteEdit" value="0" placeholder="Reste">
          </div>        
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateCom" style="font-size:15px">Modifier</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>


<!-- livraispon modal -->
<div class="modal fade" id="livrer" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Livraison et payement </h4><label id="testa"></label>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifEditCom">
        </div>
        <b>Commande du client <label id="clientlabel"></label>  montant: <label id="montantlabel"></label>  FMG , avance: <label id="avancelabel"></label>  FMG , reste: <label id="restelabel"></label>  FMG</b>
        <form id="" method=""> 
          <input type="hidden" id="idComfield">
          <div class="form-group" >
            <select class="form-control" id="modePaye">
              <option value="LIVRE-PAYE">livré et payé</option>
              <option value="LIVRE-NON PAYE">livré mais non payé</option>
            </select>
          </div>
          <div id="nonPaye">

            <div class="form-group">
               <label>Montant commande:</label>
               <i id="inetComEdit" style="color:red"></i>
               <input type="number" class="form-control" style="color:black" id="montantfield" value="0" placeholder="Prix commande (Ariary) ">
             </div>  
             <div class="form-group">
               <label>Déja payé:</label>
               <i id="iavanceEdit" style="color:red"></i>
               <input type="number" class="form-control" style="color:black" id="avancefield" value="0" placeholder="Avance">
             </div> 
             <div class="form-group">
               <label>Reste:</label>
               <i id="iresteEdit" style="color:red"></i> 
                <input type="number" class="form-control" style="color:black" id="restefield" value="0" placeholder="Reste">
             </div> 
          </div>
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " data-dismiss="modal" id="livrerCom" style="font-size:15px">Valider</button>          
        </div>
      </div>
    </form>

    </div>
  </div>
</div>

<!-- annuler modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Annulation</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <b id="titreSuppr"></b>
        <input type="hidden" id="idComAnnul">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteCom"  data-dismiss="modal" style="font-size:15px">Valider</button>          
        </div>
      </div>

    </div>
  </div>
</div>


<!-- regler modal -->
<div class="modal fade" id="regler" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Regulation</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreRegler"></p>
        <input type="hidden" id="idRegler">  <input type="hidden" id="netRegler">  
        <input type="hidden" id="resteRegler">   
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="reglerCom"  data-dismiss="modal" style="font-size:15px">Valider</button>          
        </div>
      </div>

    </div>
  </div>
</div>



<?php include('layout/footer.php') ; ?>
<?php include('ajax/commande/controlChampCommande.php') ; ?>
<?php include('ajax/commande/ajax_commande.php') ; ?>
