
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertRecouvrement">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Recouvrement </h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active cliJour" id="home-tab" data-toggle="tab" href="#" role="tab" aria-controls="" aria-selected="true">Recouvrement</a>
                      </li>
                    </ul>
                    
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="x_content">
                               <div>
                                <!--<a href="<?php echo base_url('Controller_recouvrement/action');?>">Export excel</a>
                                 --><div class="input-group pull-right" style="width:200px">
                                   <input class="form-control" type="text" id="txtRec" style="border-right:none" placeholder="Recherche année" >
                                   <span class="input-group-btn">
                                     <button type="button" id="btnRec" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                   </span>
                                 </div>
                                 <b style="color:black ; font-size:20px">RECOUVREMENT <b id="anneeRecouv"><?php echo date('Y') ; ?></b> </b><br><br>

                               </div>
                               <table id="" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                    <th colspan="7" style="padding-left:450px ; background-color:#dadada ;"><b style="color:black ; font-size:15px">ACHAT</b></th>
                                  </thead>
                                  <thead>
                                  <th>N°</th>
                                  <th>Nom du client</th>
                                  <th>Contact</th>
                                  <th>Adresse</th>
                                  <th>N° Fact</th>
                                  <th>Du(date)</th>
                                  <th>Montant (FMG)</th>
                                </thead>
                                  <tbody id="tbodyAchat">
                                  <?php if (isset($listeAchat) AND $listeAchat !=NULL ){ 
                                    $ligne  = 0 ;
                                    foreach ($listeAchat as $res) { $ligne++;    ?>
                                    <tr>
                                        <td><?php echo $ligne; ?></td>
                                        <td><?php echo $res->nomCli ; ?></td>
                                        <td><?php echo $res->telephoneCli ; ?></td>
                                        <td><?php echo $res->adresseCli ; ?></td>
                                        <td>Fact achat N° <?php echo $res->factureAchat; ?></td>
                                        <td><?php echo $res->dateAchat; ?></td>
                                        <td><?php echo $res->net ; ?></td>
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="7" style="padding-left:450px">Aucun enregistrement </td></tr>';
                                    }?>
                                 </tbody>
                                 
                                          <tfoot>
                                            <td colspan="6"><b>Total</b></td><td colspan=""><b id="totAchat"><?php echo $totalAchat?> </b></td>
                                        </tfoot>

                                 
                               </table>
                               <table id="" class="table table-striped table-bordered" style="width:100%">
                                  <thead>
                                    <th colspan="12" style="padding-left:450px ; background-color:#dadada "><b style="color:black ; font-size:15px">COMMANDE</b></th>
                                  </thead>
                                  <thead>
                                  <th>N°</th>
                                  <th>Nom du client</th>
                                  <th>Contact</th>
                                  <th>Adresse</th>
                                  <th>N° Fact</th>
                                  <th>Du(date)</th>
                                  <th>Montant (FMG)</th>
                                  <th>Avance (FMG)</th>
                                  <th>Reste (FMG)</th>
                                  <th>Emargement</th>
                                  <th>Date livraison</th>
                                  <th>Totalité récu</th>

                                </thead>
                                <tbody id="tbodyCom">
                                     <?php $ligne2 =0;  if (isset($tousCommande) AND $tousCommande !=NULL ){ 
                                    foreach ($tousCommande as $res) { $ligne2++    ?>
                                    <tr > <?php $payement =$res->emargement ;  ?>
                                        <td><?php echo $ligne2 ?></td>
                                        <td><?php echo $res->nomCli; ?></td>
                                        <td><?php echo $res->telephoneCli ; ?></td>
                                        <td><?php echo $res->adresseCli; ?></td>
                                        <td>Fact commande N°<?php echo $res->idCom; ?></td>
                                        <td><?php echo date('Y-m-d',strtotime($res->dateCom)) ?></td>
                                        <td><?php echo $res->Net ; ?></td>
                                        <td><?php echo $res->avance; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?>><?php echo $res->reste; ?></td>
                                        <td <?php if($payement=="LIVRE-NON PAYE"){ echo'style="background-color:yellow"' ; } ?> >
                                            <?php echo $res->emargement; ?>
                                        </td>
                                        <td><?php echo $res->jourLivrance; ?></td>  
                                        <td><?php echo $res->sommeRecu; ?></td>                        
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="12" style="padding-left:450px">Aucun enregistrement </td></tr>';
                                    }?>
                                 </tbody>
                                
                                          <tfoot>
                                            <td colspan="6"><b>Total</b></td>
                                            <td ><b id="totMontant"><?php echo $totalMontant ; ?> </b></td>
                                            <td> <b id="totAvance"><?php echo $totalAvance ; ?><b></td> 
                                            <td> <b id="totReste"><?php echo $totalReste ; ?><b></td> 
                                            <td colspan="2"></td>
                                            <td> <b id="totRecu"><?php echo $sommeRecu ; ?><b></td> 
                                        </tfoot>

                                
                               </table>

                         </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 
<!-- Add new modal -->
<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Ajout d'un nouveau client</h4>
        <button type="button" class="close fermerAdd" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifAdd">
        </div>
        <p>Veuillez remplir ce formulaire pour ajouter un client</p>

        <form id="usineForm" method="post"> 
          <div class="form-group">
            <i id="inom" style="color:red"></i>
            <input type="text" class="form-control" id="nomCli" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="iadresse" style="color:red"></i>
            <input type="text" class="form-control" id="adresseCli" placeholder="Adresse">
          </div>
          <div class="form-group">
            <i id="itelephone" style="color:red"></i>
            <input type="text" class="form-control" id="telephoneCli" placeholder="Telephone">
          </div>          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary" id="saveCli" style="font-size:15px">Ajouter</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



 <!-- Edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edition d'un client</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Modifier la valeur du champ pour editer un client</p>
        <form id="editForm" method="post"> 
          <div id="notifEdit"></div>
          <div class="form-group">
            <input type="hidden" class="form-control" id="idCliEdit" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="inomCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="nomCliEdit" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="iadresseCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="adresseCliEdit" placeholder="Adresse">
          </div>
          <div class="form-group">
            <i id="itelephoneCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="telephoneCliEdit" placeholder="Telephone">
          </div> 

      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateCli" style="font-size:15px">Modifier</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>

<!-- Delete modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Suppression</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreSuppr"></p>
        <input type="hidden" id="idCliDelete">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteCli"  data-dismiss="modal" style="font-size:15px">Supprimer</button>          
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('layout/footer.php') ; ?>

<!--<?php include('ajax/recouvrement/controlChampClient.php') ; ?>-->
<?php include('ajax/recouvrement/ajax_recouvrement.php') ; ?>