
<?php include('layout/header.php') ; ?>
<script>function printContent(el){
      var restorepage=document.body.innerHTML;
      var printContent=document.getElementById(el).innerHTML;
      document.body.innerHTML=printContent;
      window.print();
      document.body.innerHTML=restorepage;
    }
  </script>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertAchat">
             </div>


            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Achat </h2>  <label id="testa"> </label>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Mes achats</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Faire un nouveau achat</a>
                      </li>
                      <!--<li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                      </li>-->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                               <div>
                                 <input type="text" id="rechercheAchat" placeholder="Rechercher" style="width:200px ; margin-bottom:10px " class="form-control pull-right">
                               </div>

                              <table id="" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <th>Date</th>
                                  <th>Client</th>
                                  <th>Net</th>
                                  <th>Facture</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyAchat">
                                  <?php if (isset($listeAchat) AND $listeAchat !=NULL ){
                                    foreach ($listeAchat as $res) {    ?>
                                    <tr>
                                        <td><?php echo $res->dateAchat; ?></td>
                                        <td><?php echo $res->nomCli ; ?></td>
                                        <td><?php echo $res->net ; ?></td>
                                        <td><?php echo $res->factureAchat; ?></td>
                                        <td><button data-net="<?php echo $res->net; ?>" data-facture="<?php echo $res->factureAchat; ?>" data-date="<?php echo $res->dateAchat; ?>"  data-nom="<?php echo $res->nomCli ; ?>" data-adresse="<?php echo $res->adresseCli ; ?>" data-telephone="<?php echo $res->telephoneCli ; ?>" class="btn btn-info detailler" data-toggle="modal" data-target="#details" >Details </button>
                                            <a class="btn btn-primary" 
                                            href="<?php echo base_url('Controller_achat/facture?nomCli='.$res->nomCli.'&telephoneCli='.$res->telephoneCli.'&adresseCli='.$res->adresseCli.'&dateAchat='.$res->dateAchat.'&factureAchat='.$res->factureAchat.'&net='.$res->net.'') ; ?>">Facture</a>
                                        </td>
                                    </tr>
                                  <?php }
                                }else{

                                    echo'<tr><td colspan="5" style="padding-left:450px">Aucun enregistrement </td></tr>';
                                    }?>
                                 </tbody>
                               </table>
                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div>
                          <div class="pull-left" style=" border: 1px solid #dadada ; width:600px;"><br>
                            <b style="margin-left:20px ">les produits disponibles</b>
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%"><thead>
                                  <th>ID</th>
                                  <th>Designation</th>
                                  <th>Stock</th>
                                  <th>PU</th>
                                  <th>Quantite</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyProd">
                               
                                  <?php foreach ($listeproduit as $res) { ?>
                                    <tr>
                                         <td><?php echo $res->idProd; ?></td>
                                        <td><?php echo $res->designation; ?></td>
                                        <td><?php echo $res->stock; ?></td>
                                        <td><?php echo $res->pu; ?></td>
                                        <td>
                                        <input style="width:70px" type="number" value="0" class="quantite" id="<?php echo $res->idProd; ?>"></td>
                                        <td>
                                           <div class="btn-group" role="group" aria-label="...">                         
                                            <button type="button" data-id="<?php echo $res->idProd; ?>" data-stock="<?php echo $res->stock; ?>" data-pu="<?php echo $res->pu; ?>"  title="ajouter panier" class="addCart" >Acheter </button>
                                          </div>  
                                        </td>
                                    </tr>
                                  <?php }?>
                                
                                 </tbody>
                               </table>
                          </div>
                          <div class="pull-right col-5" style="border:1px solid #dadada"><br>
                             <div>
                              <b class="pull-left">Panier</b>
                             
                                   <div id='vider'>
                                    <?php if ($netpayer > 0 ) {
                                      ?> 
                                        <button class="pull-right" style="height:30px" id="viderPanier">Vider le panier</button>
                                      <?php
                                    } ?>
                                   </div>
                             </div>
                              <table  class="table table-striped " ><thead>
                                  <th>Produit</th>
                                  <th>Qte</th>
                                  <th>Subtotal</th>
                                  <th>Actions</th>
                                </thead>
                                <tbody id="tbodyPan">
                                  <?php $ligne =0 ;  foreach ($listepanier as $res) { $ligne++ ;   ?>
                                    <tr>
                                        <td><?php echo $res->designation; ?></td>
                                        <td><?php echo $res->qtePanier; ?></td>
                                        <td><?php echo $res->subtotalPanier; ?></td>
                                        <td>
                                           <div class="btn-group" role="group" aria-label="...">                         
                                            <button title="augmenter" type="button" data-id='<?php echo $res->idProd; ?>'  data-stock='<?php echo $res->stock ?>' data-pu='<?php echo $res->pu ?>' data-designation='<?php echo $res->designation ?>' data-toggle="modal" data-target="#editPlus" class="btn btn-info btn-xs editPlus" ><i class="glyphicon glyphicon-plus"></i></button>
                                            <button title="diminuer" type="button" data-id='<?php echo $res->idProd; ?>'  data-stock='<?php echo $res->stock ?>' data-pu='<?php echo $res->pu ?>' data-quantitepanier='<?php echo $res->qtePanier ?>' data-designation='<?php echo $res->designation ?>'  data-toggle="modal" data-target="#editMoins" class="btn btn-warning btn-xs editMoins"><i class="glyphicon glyphicon-minus"></i></button>
                                            <button title="annuler" type="button" data-id='<?php echo $res->idProd; ?>' data-quantitepanier='<?php echo $res->qtePanier ?>' data-toggle="modal" data-target="#delete"  class="btn btn-danger btn-xs delete"><i class="glyphicon glyphicon-trash"></i></button>
                                           </div>  
                                        </td>
                                    </tr>
                                  <?php }?>

                                   <?php if($ligne==0){
                                      echo'<tr><td colspan="4" style="padding-left:120px">Le panier est vide pour le moment</td></tr>';
                                    } ?> 
                                    <tfoot id="net">
                                    <?php if($netpayer > 0){
                                      ?><tr><td colspan="4" style="">Ney à payer: <?php echo $netpayer ?> Ariary</td>

                                      </tr>
                                          <tr><td colspan="4">
                                              <select class="form-control" style="" id="idClient">
                                              <option  selected disabled="">--------Choisir le client puis valider achat----------</option>
                                              <?php foreach ($listeclient as $res) { ?>
                                                  <option value="<?php echo $res->idCli; ?>"><?php echo $res->nomCli; ?></option>
                                              <?php }?>
                                              </select><br>
                                              <button  id="enregistrerAchat" class="btn btn-primary" style="margin-left:130px"  id="">Valider achat</button>
                                            </td>
                                          </tr>
                                      <?php


                                           
                         
                                    } ?> 
                                    </tfoot>
                                 </tbody>
                               </table>
                               
                          </div>
                        </div>
                      </div>
                      <!--<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                            booth letterpress, commodo enim craft beer mlkshk 
                      </div>-->
                    </div>
                  </div>
                  

                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 


 <!-- Edit modal -->
<div class="modal fade" id="editPlus" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Additionner la quantité</h4>
        <button type="button" class="close fermerEditPlus" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Entrer un valeur à additionner pour le produit  <label id="design"></label></p>
        <form id="editForm" method="post"> 
          <div id="notifEditPlus"></div>
          <div class="form-group">
            <i id="iqtePlus" style="color:red"></i>
            <input type="number" class="form-control quantite" id="qtePlus" placeholder="Quantité à additionner" value="0">
            <input type="hidden" id="idProd"/><input type="hidden" id="prix"/><input type="hidden" id="stockage"/>
          </div> 

      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEditPlus" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updatePlus" style="font-size:15px">Valider</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>


<div class="modal fade" id="editMoins" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Soustraire la quantité</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">         
        <p>Entrer un valeur à soustraire  pour le produit   <label id="designMoins"></label> </p>
        <form id="editForm" method="post"> 
          <div id="notifEditMoins"></div>
          <div class="form-group">
            <i id="iqteMoins" style="color:red"></i>
            <input type="number" class="form-control quantite" id="qteMoins" placeholder="Quantité à soustraire">
            <input type="hidden" id="idProdMoins"/><input type="hidden" id="prixMoins"/><input type="hidden" id="qtePanier"/>
         
          </div> 

      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEditMoins" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateMoins" style="font-size:15px">Enregistrer</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>


<!-- Details modal -->
<div class="modal fade" id="details" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Details de l'achat</h4>
        <button type="button" class="close fermerAdd" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div>
           <div><!--entete-->
              <ul class="stats-overview">
             <li>
                <span>Atelier valtolahy <br> Fianarantsoa <br> 034 25 771 00</span>
              </li>
              <li>
                <span id="nomDet"></span> <br> <span id="adresseDet">adresse</span> <br> <span id="telephoneDet">telephone</span>
              </li>
              <li class="hidden-phone">
                <span class="name"> Achat le <span id="dateDet"> </span> <br> </span></span>
                
              </li>
            </ul>
           </div><br>
           <b style="margin-left:160px">facture Achat N° <span id="factDet"> </span></b> <br><br>
           <div>
             <table class="table table-striped">
               <thead>
                 <th>produit</th>
                 <th>Qte</th>
                 <th>Subtotal</th>
               </thead>
               <tbody id="tbodyDetails">
                 
               </tbody>
               <tfoot>
                 <tr>
                   <td colspan="2"></td>
                   <td > <b>Total :</b><b id="netDet"></b></td>
                 </tr>
               </tfoot>
             </table>
           </div> 
                   
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



<?php include('layout/footer.php') ; ?>
<?php include('ajax/achat/ajax_achat.php') ; ?>
<?php include('ajax/achat/controlChampAchat.php') ; ?>