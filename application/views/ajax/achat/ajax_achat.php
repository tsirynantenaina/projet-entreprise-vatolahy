<script>
//enregistrer achat
$(document).on('click', '#enregistrerAchat',  function(){
  
   var id = $('#idClient').val();
   if(id !== null){
    if(confirm('Enregistrer ?')){
   		$.ajax({
           url:"<?php echo base_url('Controller_achat/enregister') ; ?>",
           method:"POST",
           data:{idCli:id},
           success:function(html){
           	  $('#tbodyAchat').html(html);
               actualiser();
           	  alertAchat.innerHTML ='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enregistrement achat réussie</div>'
           	  
           } 
         });

    }
   }else{
     alert('Veuillez choisir un client');
   	}
   
});

//add panier
$(document).on('click', '.addCart',  function(){
   var id = $(this).data('id');
   var stock= $(this).data('stock');
   var pu = $(this).data('pu');
   var quantite = $('#'+id).val();
   if( quantite !=="" && quantite > 0){
   		var saisi = parseInt(quantite);
   		var austock = parseInt(stock);
   	  if(saisi < austock){
   	  	 var subtotal = parseInt(pu) * parseInt(quantite);
   	  	 $.ajax({
   	  	   //ajout  au panier  et stock moins
           url:"<?php echo base_url('Controller_achat/panier') ; ?>",
           method:"POST",
           data:{idProd:id, quantite:quantite, subtotal:subtotal},
           success:function(html){

           	actualiser(); 

           } 
         });
   	  }else{ alert('Notre stock est insuffisant') ; }
   }else{
   	 alert('Veuillez entrer une quantite exacte');
   }
});


//vider panier 
$(document).on('click', '#viderPanier',  function(){
   	if(confirm('Voulez-vous vraiment vider panier')){
   	   $.ajax({
   	  	   //vider le panier
           url:"<?php echo base_url('Controller_achat/vider') ; ?>",
           method:"POST",
           data:{},
           success:function(html){
           		actualiser();
           } 
         });
   	}
 });

//get valeur additon 
$(document).on('click', '.editPlus',  function(){
   	var id= $(this).data('id'); $('#idProd').val(id); 
   	var pu = $(this).data('pu');$('#prix').val(pu); 
    var stock = $(this).data('stock');$('#stockage').val(stock); 
   	var designation= $(this).data('designation');
   	design.innerHTML = designation;
   	
 });

//valider l'addition
$(document).on('click', '#updatePlus',  function(){
   	var id= $('#idProd').val(); 
   	var pu= $('#prix').val(); 
   	var quantite = $('#qtePlus').val();
   	var stock = $('#stockage').val();
   	if(quantite !=="" && quantite > 0 ){
   		if(quantite < stock){
   		   $.ajax({
             url:"<?php echo base_url('Controller_achat/addition') ; ?>",
             method:"POST",
             data:{idProd:id , pu:pu , quantite:quantite},
             success:function(html){
             		 notifEditPlus.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification quantité réussie</div>';
             		 actualiser();
             		 $('#qtePlus').val('0');
             } 
            });
   		}else{
   			notifEditPlus.innerHTML='<div class="alert alert-danger alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Stock insuffisant , nous n\'avons que '+stock+' en stock </div>';         		 
   		}
   		
   	}else{
   		iqtePlus.innerHTML='Veuillez remplir ce champ';
   		qtePlus.style.border="1px solid red";
    }
   	
 });


//get soustraction
$(document).on('click', '.editMoins',  function(){
   	var id= $(this).data('id'); $('#idProdMoins').val(id); 
   	var pu = $(this).data('pu');$('#prixMoins').val(pu);
    var qtePanier = $(this).data('quantitepanier');$('#qtePanier').val(qtePanier); 
   	var designation= $(this).data('designation');
   	designMoins.innerHTML = designation;
   	
 });

//valider l'addition
$(document).on('click', '#updateMoins',  function(){
   	var id= $('#idProdMoins').val(); 
   	var pu= $('#prixMoins').val(); 
   	var quantite = $('#qteMoins').val();
   	var qtePanier = $('#qtePanier').val();
   	if(quantite !=="" && quantite > 0 ){
   		var saisi = parseInt(quantite);
   		var aupanier = parseInt(qtePanier);
   		if(saisi < aupanier){
   		   $.ajax({
             url:"<?php echo base_url('Controller_achat/soustraction') ; ?>",
             method:"POST",
             data:{idProd:id , pu:pu , quantite:quantite},
             success:function(html){
             	$('#qteMoins').val('0');
                notifEditMoins.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification quantité réussie</div>';
                actualiser();
             } 
           });
   		}else if(saisi === aupanier){
   			notifEditMoins.innerHTML='<div class="alert alert-danger alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Veuillez tapez un valeur inferieur à '+aupanier+'</div>';         		 
   		
   		}else{
   			notifEditMoins.innerHTML='<div class="alert alert-danger alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Impossible de sousstraire '+saisi+' car vous n\'avez que '+aupanier+' au panier</div>';         		 
   		}
   		
   	}else{
   		iqteMoins.innerHTML='Veuillez remplir ce champ';
   		qteMoins.style.border="1px solid red";
    }
   	
 });

//annuler
$(document).on('click', '.delete',  function(){
	if(confirm('Voulez-vous supprimer ce produit dans le panier?')){
	 var id = $(this).data('id');
	 var quantite = $(this).data('quantitepanier');
	 $.ajax({
       url:"<?php echo base_url('Controller_achat/annuler') ; ?>",
       method:"POST",
       data:{idProd:id , quantite:quantite},
       success:function(html){
         actualiser(); 
       } 
     });
	}
})



function actualiser(){
  $.ajax({
  	//actualiser produit
    url:"<?php echo base_url('Controller_achat/actualisationProduit') ; ?>",
    method:"POST",
    data:{},
    success:function(html){
        $('#tbodyProd').html(html);
    }
  }) ;
  $.ajax({
       url:"<?php echo base_url('Controller_achat/actualisationPanier') ; ?>",
       method:"POST",
       data:{},
       success:function(html){
         $('#tbodyPan').html(html);
       }
  }) ;
                 
   $.ajax({
     url:"<?php echo base_url('Controller_achat/actualisationNet') ; ?>",
     method:"POST",
     data:{},
     success:function(html){
       $('#net').html(html);
     }
  }) ;
                        
    $.ajax({
      url:"<?php echo base_url('Controller_achat/actualisationBoutonVider') ; ?>",
      method:"POST",
      data:{},
      success:function(html){
        $('#vider').html(html); 
      } 
    });
                       
         
}


$(document).on('keyup', '#rechercheAchat',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_Achat/rechercher') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyAchat').html(html);           
        } 
   });
    
    
});



$(document).on('click', '.detailler',  function(){
   var nom = $(this).data('nom');
   var adresse = $(this).data('adresse');
   var telephone = $(this).data('telephone');
   var date = $(this).data('date');
   var facture = $(this).data('facture');
   var net = $(this).data('net');
   nomDet.innerHTML=''+nom+'';
   adresseDet.innerHTML=''+adresse+'';
   telephoneDet.innerHTML=''+telephone+'';
   dateDet.innerHTML=''+date+'';
   factDet.innerHTML=''+facture+'';
   netDet.innerHTML=''+net+'';
    $.ajax({
        url:"<?php echo base_url('Controller_Achat/details') ; ?>",
        method:"POST",
        data:{id:facture},
        success:function(html){
          $('#tbodyDetails').html(html);           
        }
   });
});
	
</script>