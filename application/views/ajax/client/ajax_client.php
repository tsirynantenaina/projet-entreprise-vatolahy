<script type="text/javascript">
	
   	//bouton inserer
    $(document).on('click', '#saveCli',  function(){
    var nom= $('#nomCli').val();
    var adresse = $('#adresseCli').val();
     var telephone = $('#telephoneCli').val();
    if(nom !=="" && adresse !=="" && telephone !==""){
      $.ajax({
        url:"<?php echo base_url('Controller_client/inserer') ; ?>",
        method:"POST",
        data:{nom:nom , adresse:adresse ,  telephone:telephone},
        success:function(html){
          $('#tbodyCli').html(html);
          notifAdd.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Insertion réussie</div>';
          $('#nomCli').val('');$('#adresseCli').val('');$('#telephoneCli').val('');
          alertClient.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Insertion client reussie</div>';
          
        } 
     });
    }
 });

//get edit
$(document).on('click', '.editCli',  function(){
   var id = $(this).data('id'); $('#idCliEdit').val(id);
   var nom = $(this).data('nom');$('#nomCliEdit').val(nom);
   var adresse = $(this).data('adresse');$('#adresseCliEdit').val(adresse);
   var telephone = $(this).data('telephone');$('#telephoneCliEdit').val(telephone);
 });

//bouton modifier
    $(document).on('click', '#updateCli',  function(){
    var id= $('#idCliEdit').val();
    var nom= $('#nomCliEdit').val();
    var adresse = $('#adresseCliEdit').val();
     var telephone = $('#telephoneCliEdit').val();
    if(nom !=="" && adresse !=="" && telephone !=="" && id !==""){
      $.ajax({
        url:"<?php echo base_url('Controller_client/modifier') ; ?>",
        method:"POST",
        data:{id:id , nom:nom , adresse:adresse ,  telephone:telephone},
        success:function(html){
          $('#tbodyCli').html(html);
          notifEdit.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification réussie</div>';
          //$('#nomCli').val('');$('#adresseCli').val('');$('#telephoneCli').val('');
          alertClient.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Modification client reussie</div>';
          
        } 
     });
    }
 });

//get id à suppr
  $(document).on('click', '.delete',  function(){
    var id = $(this).data('id');
    $('#idCliDelete').val(id);
    titreSuppr.innerHTML='Attention, tous les données liées à ceci vont toutes disparaitre!! Voulez-vous vraiment supprimer client numero '+id+' ?' ; 
    
 });

  $(document).on('click', '#deleteCli',  function(){
    var id= $('#idCliDelete').val();
    if(id !=="" ){
      $.ajax({
        url:"<?php echo base_url('Controller_client/supprimer') ; ?>",
        method:"POST",
        data:{id:id},
        success:function(html){
          $('#tbodyCli').html(html);
          alertClient.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Suppression client reussie</div>';
          
        } 
     });
    }
 });

$(document).on('keyup', '#rechercheCli',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_client/rechercher') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyCli').html(html);           
        } 
    });
    
    
});

$(document).on('click', '#ficheCli',  function(){
   // var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_client/action') ; ?>",
        method:"POST",
        data:{},
        success:function(html){          
        } 
    });
    
    
});



</script>