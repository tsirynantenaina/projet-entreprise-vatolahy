<script type="text/javascript">
	
   	//bouton inserer
    $(document).on('click', '#saveDepense',  function(){
    var action= "DEPENSE";
    var operation= $('#operation').val();
    var somme= $('#somme').val();
    if(action !=="" && operation !=="" && somme!==""){
      $.ajax({
        url:"<?php echo base_url('Controller_depense/inserer') ; ?>",
        method:"POST",
        data:{action:action , operation:operation ,  somme:somme},
        success:function(html){
          $('#tbodyDepense').html(html);
          notifAdd.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Insertion réussie</div>';
          $('#action').val('');$('#operation').val('');$('#somme').val('');
          alertDepense.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Insertion dépense reussie</div>';
          
        } 
     });
    }
 });


//get edit
$(document).on('click', '.editDepense',  function(){
   var id = $(this).data('id'); $('#idDepense').val(id);
   var operation = $(this).data('operation');$('#operationEdit').val(operation);
   var somme = $(this).data('somme');$('#sommeEdit').val(somme);
 });

//bouton modifier
    $(document).on('click', '#updateDepense',  function(){
    var id= $('#idDepense').val();
    var operation= $('#operationEdit').val();
    var somme = $('#sommeEdit').val();
    if(id !=="" && operation !=="" && somme !=="" ){
      $.ajax({
        url:"<?php echo base_url('Controller_depense/modifier') ; ?>",
        method:"POST",
        data:{id:id , operation:operation , somme:somme },
        success:function(html){
          $('#tbodyDepense').html(html);
          notifEdit.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification réussie</div>';
          alertDepense.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Modification dépense reussie</div>';
          
        } 
     });
    }
 });

  $(document).on('click', '.delete',  function(){
    var id = $(this).data('id');
     var operation = $(this).data('operation');
     var somme = $(this).data('somme');
    $('#idDelete').val(id);
    titreSuppr.innerHTML='Supprimer ce depense '+operation+' ('+somme+') ?' ; 
    
 });


    $(document).on('click', '#deleteDepense',  function(){
    var id= $('#idDelete').val();
    if(id !=="" ){
      $.ajax({
        url:"<?php echo base_url('Controller_depense/supprimer') ; ?>",
        method:"POST",
        data:{id:id},
        success:function(html){
          $('#tbodyDepense').html(html);
          alertDepense.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Suppression dépense reussie</div>';
          
        } 
     });
    }
 });

    $(document).on('keyup', '#rechercheDepense',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_depense/rechercher') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyDepense').html(html);           
        } 
    });
    
    
});


</script>