<script type="text/javascript">
    $(document).on('click', '.btnConnecter',  function(){
    var nomUtilisateur = $('#pseudo').val();
    var motDePasse = $('#mdp').val();
    if(nomUtilisateur !=="" && motDePasse !==""){
      $.ajax({
        url:"<?php echo base_url('Controller_login/entrer') ; ?>",
        method:"POST",
        data:{pseudo:nomUtilisateur , mdp:motDePasse},
        success:function(data){
          if(data =="correct"){
          	//messageAlert.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Information correcte </div>';
          	window.location="<?php  echo base_url('Controller_acceuil') ; ?>";
          }if(data == "incorrect"){
          	messageAlert.innerHTML='<div class="alert alert-danger alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Pseudo ou mot de passe incorrect</div>';
       
          }
        } 
     });
    }
 });
</script>
