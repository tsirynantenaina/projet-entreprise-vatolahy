<script type="text/javascript">


$(document).on('click', '#saveCom',  function(){
    var commande =$('#com').val();
    var iliv= idateLiv.innerHTML;
    var liv= $('#dateLiv').val();
    var dt = liv.substring(6,10)+'-'+liv.substring(3,5)+'-'+liv.substring(0,2) ; 
    var tot= $('#netCom').val();
    var av= $('#avance').val();
    var rest= $('#reste').val();
    var  idCli= $('#idClient').val();
    if(iliv ==="" && tot !=="" && av !=="" && rest !=="" && commande !=="" && commande !==null){
        $.ajax({
        url:"<?php echo base_url('Controller_commande/inserer') ; ?>",
        method:"POST",
        data:{idCli:idCli , commande:commande , dateLiv:dt , montant:tot , avance:av , reste:rest},
        success:function(html){
            $('#tbodyCom').html(html);actualiser();
            notifAddCom.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Commande bien enregistrer</div>';
            alertCommande.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Insertion commande réussie</div>';
            $('#dateLiv').val('');$('#netCom').val(''); $('#avance').val('');  $('#reste').val(''); $('#com').val('');
        }
         
     });
    }
});


//get edit commande

$(document).on('click', '.editCom',  function(){
    var  idCom = $(this).data('idcom');$('#idComEdit').val(idCom);
    var  idClient = $(this).data('idcli');
    var  commande = $(this).data('commande'); $('#comEdit').val(commande);
    var  livraison= $(this).data('livraison'); $('#dateLivEdit').val(''+livraison.substring(8,10)+'-'+livraison.substring(5,7)+'-'+livraison.substring(0,4)+'');
    var  net= $(this).data('net'); $('#netComEdit').val(net);
    var  avance= $(this).data('avance'); $('#avanceEdit').val(avance);
    var  reste= $(this).data('reste');  $('#resteEdit').val(reste);
     $.ajax({
        url:"<?php echo base_url('Controller_commande/getCli') ; ?>",
        method:"POST",
        data:{idCli:idClient},
        success:function(html){
            $('#idClientEdit').html(html);
        }
     });

});

//edier commande 
$(document).on('click', '#updateCom',  function(){
    var commande =$('#comEdit').val();
    var iliv= idateLivEdit.innerHTML;
    var liv= $('#dateLivEdit').val();
    var dt = liv.substring(6,10)+'-'+liv.substring(3,5)+'-'+liv.substring(0,2) ; 
    var tot= $('#netComEdit').val();
    var av= $('#avanceEdit').val();
    var rest= $('#resteEdit').val();
    var  idCli= $('#idClientEdit').val();
    var  idCom= $('#idComEdit').val();
    if(iliv ==="" && idCom!=="" && tot !=="" && av !=="" && rest !=="" && commande !=="" && commande !==null){
        $.ajax({
        url:"<?php echo base_url('Controller_commande/modifier') ; ?>",
        method:"POST",
        data:{idCom:idCom , idCli:idCli , commande:commande , dateLiv:dt , montant:tot , avance:av , reste:rest},
        success:function(html){
            $('#tbodyCom').html(html);
            notifEditCom.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Commande bien modifié</div>';
            alertCommande.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification commande réussie</div>';
            //$('#dateLiv').val('');$('#netCom').val(''); $('#avance').val('');  $('#reste').val(''); $('#com').val('');
        }
         
     });
    }
});


//get livraison
$(document).on('click', '.livraison',  function(){
    nonPaye.style.display="none";
    var idCom = $(this).data('idcom'); 
    var nom =  $(this).data('nom');  clientlabel.innerHTML=''+nom+'' ; 
    var montant =  $(this).data('montant');  montantlabel.innerHTML=''+montant+'' ; 
    var reste =  $(this).data('reste');  restelabel.innerHTML=''+reste+'' ; 
    var avance =  $(this).data('avance');  avancelabel.innerHTML=''+avance+'' ; 
    $('#montantfield').val(montant);
    $('#restefield').val(reste);
    $('#avancefield').val(avance);
    $('#idComfield').val(idCom);
    $.ajax({
          url:"<?php echo base_url('Controller_commande/modePayer') ; ?>",
          method:"POST",
          data:{},
          success:function(html){
           $('#modePaye').html(html);
         }
     }); 
    

})


//affichage et disparition

$(document).on('change', '#modePaye',  function(){
    var val = $(this).val();
    if(val === 'LIVRE-NON PAYE'){
        nonPaye.style.display="block";
    }if(val === 'LIVRE-PAYE'){
        nonPaye.style.display="none";
    }
})
 
function actualiser(){
    $.ajax({
                url:"<?php echo base_url('Controller_commande/actualiserpl') ; ?>",
                method:"POST",
                data:{},
                success:function(html){
                 $('#tbodypl').html(html);
                     $.ajax({
                          url:"<?php echo base_url('Controller_commande/nonpayer') ; ?>",
                          method:"POST",
                          data:{},
                          success:function(html){
                           $('#tbodypnl').html(html);
                                $.ajax({
                                    url:"<?php echo base_url('Controller_commande/decommande') ; ?>",
                                    method:"POST",
                                    data:{},
                                    success:function(html){
                                     $('#tbodydcd').html(html);
                                      $.ajax({
                                          url:"<?php echo base_url('Controller_commande/tous') ; ?>",
                                          method:"POST",
                                          data:{},
                                          success:function(html){
                                           $('#tbodytous').html(html);
                                           $.ajax({
                                                 url:"<?php echo base_url('Controller_commande/facture') ; ?>",
                                                 method:"POST",
                                                 data:{},
                                                 success:function(html){
                                                  $('#tbodyFact').html(html);
                                                 }
                                              });
                                          }
                                      });

                                    }
                                });

                          }
                      });          
                }
            });
}
//livrer et payer

$(document).on('click', '#livrerCom',  function(){
    var idCom = $('#idComfield').val();
    var mode = $('#modePaye').val();
    var reste = $('#restefield').val();
    var avance = $('#avancefield').val();
    var montant = $('#montantfield').val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/livrer') ; ?>",
        method:"POST",
        data:{idCom:idCom , mode:mode , reste:reste , avance:avance , montant:montant},
        success:function(html){
           $('#tbodyCom').html(html);
           actualiser();
           alertCommande.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Réussie</div>';
                                 
        }
     });
})


//get annuler une commande
$(document).on('click', '.delete',  function(){
    var nom = $(this).data('nom');
    var idCom = $(this).data('idcom');$('#idComAnnul').val(idCom);
    titreSuppr.innerHTML='Annuler la commande de '+nom+'?';
})

//get annuler une commande
$(document).on('click', '#deleteCom',  function(){
    var idCom =$('#idComAnnul').val();
    if(idCom !==""){
         $.ajax({
          url:"<?php echo base_url('Controller_commande/annuler') ; ?>",
          method:"POST",
          data:{idCom:idCom},
          success:function(html){
           $('#tbodyCom').html(html);
             actualiser();
             alertCommande.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Commande annulée</div>';
          }
        });
    }
})


//get regler
$(document).on('click', '.regler',  function(){
    var nom = $(this).data('nom');
    var net = $(this).data('net');$('#netRegler').val(net);
    var idCom = $(this).data('idcom');$('#idRegler').val(idCom);
    var reste = $(this).data('reste');$('#resteRegler').val(reste);
    titreRegler.innerHTML='Le client '+nom+' à payé la totalité du reste à payer?  ';
})


//regler commande 
$(document).on('click', '#reglerCom',  function(){
    var idCom =$('#idRegler').val();
    var net =$('#netRegler').val();
    var reste =$('#resteRegler').val();
    if(idCom !==""){
         $.ajax({
          url:"<?php echo base_url('Controller_commande/regler') ; ?>",
          method:"POST",
          data:{idCom:idCom , net:net , reste:reste},
          success:function(html){
           $('#tbodyCom').html(html);
             actualiser();
             alertCommande.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Commande reglée</div>';
          }
        });
    }
}) ; 


//recherche commmande 
$(document).on('keyup', '#rechercheCom',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherCom') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyCom').html(html);           
        } 
    });  
});

$(document).on('keyup', '#recherchePayer',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherPayer') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodypl').html(html);           
        } 
    });  
});

$(document).on('keyup', '#rechercheNonPayer',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherNonPayer') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodypnl').html(html);           
        } 
    });  
});

$(document).on('keyup', '#rechercheDecom',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherDecom') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodydcd').html(html);           
        } 
    });  
});

$(document).on('keyup', '#recherchetous',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherTous') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodytous').html(html);           
        } 
    });  
});

$(document).on('keyup', '#rechercheFact',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_commande/rechercherFact') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyFact').html(html);           
        } 
    });  
});



</script>