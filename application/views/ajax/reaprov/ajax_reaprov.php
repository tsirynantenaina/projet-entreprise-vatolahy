<script type="text/javascript">
  
    //bouton inserer
$(document).on('click', '#reaprovProd',  function(){
    var id = $('#idProd').val();
    var qteReaprov = $('#qteReaprov').val();
    $.ajax({
        url:"<?php echo base_url('Controller_reaprov/reaprov') ; ?>",
        method:"POST",
        data:{id:id , qteReaprov:qteReaprov},
        success:function(html){
          $('#tbodyProd').html(html); 
           notifEdit.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Reaprovision réussie</div>';
          //$('#nomCli').val('');$('#adresseCli').val('');$('#telephoneCli').val('');
          alertProduit.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Reaprov produit reussie</div>';
          
           
        } 
   });
 });

//get edit
$(document).on('click', '.editProd',  function(){
   var id = $(this).data('id'); $('#idProd').val(id);
   var designation = $(this).data('designation');
   nomProd.innerHTML=designation;
 });

$(document).on('keyup', '#rechercheProd',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_reaprov/rechercher') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyProd').html(html);           
        } 
   });    
});




</script>