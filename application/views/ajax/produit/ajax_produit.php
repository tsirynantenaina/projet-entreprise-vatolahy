<script type="text/javascript">
  
    //bouton inserer
    $(document).on('click', '#saveProd',  function(){
    var designation = $('#designation').val();
    var stock = $('#stock').val();
     var pu = $('#pu').val();
    if(designation !=="" && stock !=="" && pu !==""){
      $.ajax({
        url:"<?php echo base_url('Controller_produit/inserer') ; ?>",
        method:"POST",
        data:{designation:designation, stock:stock,  pu:pu},
        success:function(html){
          $('#tbodyProd').html(html);
          notifAdd.innerHTML='<div class="alert alert-success alert-dismissible role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Insertion réussie</div>';
          $('#designation').val('');$('#stock').val('');$('#pu').val('');
          alertProduit.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Insertion produit reussie</div>';
          $.ajax({
            url:"<?php echo base_url('Controller_produit/actualiserReap') ; ?>",
            method:"POST",
            data:{},
            success:function(html){
              $('#tbodyProdReap').html(html);           
            } 
        }); 
        } 
     });
    }
 });

//get edit
$(document).on('click', '.editProd',  function(){
   var id = $(this).data('id'); $('#idProdEdit').val(id);
   var designation = $(this).data('designation');$('#designationProdEdit').val(designation);
   var stock = $(this).data('stock');$('#stockProdEdit').val(stock);
   var pu = $(this).data('pu');$('#puProdEdit').val(pu);
 });

//bouton modifier
  $(document).on('click', '#updateProd',  function(){
    var id= $('#idProdEdit').val();
    var designation= $('#designationProdEdit').val();
    var stock= $('#stockProdEdit').val();
    var pu = $('#puProdEdit').val();
    if(pu !=="" && designation !=="" && stock !=="" && id !==""){
      $.ajax({
        url:"<?php echo base_url('Controller_produit/modifier') ; ?>",
        method:"POST",
        data:{id:id , designation:designation, stock:stock,  pu:pu},
        success:function(html){
          $('#tbodyProd').html(html);
          notifEdit.innerHTML='<div class="alert alert-success alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Modification réussie</div>';
          //$('#nomCli').val('');$('#adresseCli').val('');$('#telephoneCli').val('');
          alertProduit.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Modification produit reussie</div>';
          
          $.ajax({
            url:"<?php echo base_url('Controller_produit/actualiserReap') ; ?>",
            method:"POST",
            data:{},
            success:function(html){
              $('#tbodyProdReap').html(html);           
            } 
        }); 
        } 
    });
  }
 });

//get id à suppr
  $(document).on('click', '.delete',  function(){
    var id = $(this).data('id');
    
 });

  $(document).on('click', '#deleteProd',  function(){
    var id= $('#idProdDelete').val();
    if(id !=="" ){
      $.ajax({
        url:"<?php echo base_url('Controller_produit/supprimer') ; ?>",
        method:"POST",
        data:{id:id},
        success:function(html){
          $('#tbodyProd').html(html);
          alertProduit.innerHTML='<div class="alert alert-warning alert-dismissible " role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Suppression produit reussie</div>';
          $.ajax({
            url:"<?php echo base_url('Controller_produit/actualiserReap') ; ?>",
            method:"POST",
            data:{},
            success:function(html){
              $('#tbodyProdReap').html(html);           
            } 
        }); 
        } 
     });
    }
 });

$(document).on('keyup', '#rechercheProd',  function(){
    var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_produit/rechercher') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyProd').html(html);           
        } 
   });    
});

$(document).on('keyup', '#rechercheProdReap',  function(){
   var saisi = $(this).val();
    $.ajax({
        url:"<?php echo base_url('Controller_produit/rechercherReap') ; ?>",
        method:"POST",
        data:{saisi:saisi},
        success:function(html){
          $('#tbodyProdReap').html(html);           
        } 
   });    
});

</script>