
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->
        
        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title"></div>

            <div class="clearfix"></div>
             <div id="alertClient">
             </div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 >Compte</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  
                  

                  <div class="x_content">
                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active cliJour" id="home-tab" data-toggle="tab" href="#" role="tab" aria-controls="" aria-selected="true">Compte annuel</a>
                      </li>
          
                    </ul>
                    
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="x_content">
                               <div>
                                 <div class="input-group pull-right" style="width:200px">
                                   <input class="form-control" type="text" id="txtRec" style="border-right:none" placeholder="Recherche année" >
                                   <span class="input-group-btn">
                                     <button type="button" id="btnRec" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                   </span>
                                 </div>
                                 <b style="color:black ; font-size:20px">COMPTE <b id="anneeCompte"><?php echo date('Y') ; ?></b> </b><br><br>

                               </div>
                               <table id="" class="table table-striped table-bordered" style="width:100%" border="1px solid black">
                                   <thead>
                                   <th>Date</th>
                                  <th>Facture </th>
                                  <th>ENTREE</th>
                                  <th>SORTIE</th>
                                  <th>OPERATION</th>
                                </thead>
                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>JANVIER</b></td></tr>
                                      <?php if (isset($compteJanvier) AND $compteJanvier !=NULL){
                                         foreach ($compteJanvier as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerJanvier ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirJanvier ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerJanvier - $sortirJanvier ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>
                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de janvier</td></tr>';
                                    }
                                  ?>  
                                </tbody>
                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>FEVRIER</b></td></tr>
                                      <?php if (isset($compteFevrier) AND $compteFevrier !=NULL){
                                         foreach ($compteFevrier as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerFevrier ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirFevrier ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerFevrier - $sortirFevrier ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                        
                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de fevrier</td></tr>';
                                    }
                                  ?>  
                                </tbody>
                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>MARS</b></td></tr>
                                      <?php if (isset($compteMars) AND $compteMars !=NULL){
                                         foreach ($compteMars as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerMars ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirMars ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerMars - $sortirMars ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                         Mars
                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de Mars</td></tr>';
                                    }
                                  ?>  
                                </tbody>
                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>AVRIL</b></td></tr>
                                      <?php if (isset($compteAvril) AND $compteAvril !=NULL){
                                         foreach ($compteAvril as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerAvril ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirAvril ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerAvril - $sortirAvril ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de Avril</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>MAI</b></td></tr>
                                      <?php if (isset($compteMai) AND $compteMai !=NULL){
                                         foreach ($compteMai as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerMai ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirMai ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerMai - $sortirMai ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de mai</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>JUIN</b></td></tr>
                                      <?php if (isset($compteJuin) AND $compteJuin !=NULL){
                                         foreach ($compteJuin as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerJuin ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirJuin ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerJuin - $sortirJuin ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de juin</td></tr>';
                                    }
                                  ?>  
                                </tbody>
                                
                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>JUILLET</b></td></tr>
                                      <?php if (isset($compteJuillet) AND $compteJuillet !=NULL){
                                         foreach ($compteJuillet as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerJuillet ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirJuillet ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerJuillet - $sortirJuillet ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de juillet</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>AOUT</b></td></tr>
                                      <?php if (isset($compteAout) AND $compteAout !=NULL){
                                         foreach ($compteAout as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerAout ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirAout ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerAout - $sortirAout ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de juillet</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>SEPTEMBRE</b></td></tr>
                                      <?php if (isset($compteSeptembre) AND $compteSeptembre !=NULL){
                                         foreach ($compteSeptembre as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerSeptembre ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirSeptembre ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerSeptembre - $sortirSeptembre ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de septembre</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>OCTOBRE</b></td></tr>
                                      <?php if (isset($compteOctobre) AND $compteOctobre !=NULL){
                                         foreach ($compteOctobre as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerOctobre ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirOctobre ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerOctobre - $sortirOctobre ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de octobre</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>NOVEMBRE</b></td></tr>
                                      <?php if (isset($compteNovembre) AND $compteNovembre !=NULL){
                                         foreach ($compteNovembre as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerNovembre ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirNovembre ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerNovembre - $sortirNovembre ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de novembre</td></tr>';
                                    }
                                  ?>  
                                </tbody>

                                <tbody id="tbodyJanv">
                                    <tr><td style="padding-left:450px ; background-color:orange ;" colspan="5"><b>DECEMBRE</b></td></tr>
                                      <?php if (isset($compteDecembre) AND $compteDecembre !=NULL){
                                         foreach ($compteDecembre as $res) { ?>
                                            <tr>
                                                <td><?php echo $res->dateCompte; ?></td>
                                                <td><?php echo $res->factureEntre; ?></td>
                                                <td><?php echo $res->sommeEntre; ?></td>
                                                <td><?php echo $res->sommeCompte; ?></td>
                                                <td><?php echo $res->operation; ?></td>
                                            </tr>
                                         <?php } 
                                         ?>
                                          <tr><td colspan="2"></td> 
                                              <td style="background-color:red"><b><?php echo $entrerDecembre ; ?> </b></td>
                                              <td style="background-color:green"><b><?php echo $sortirDecembre ; ?></b></td>
                                              <td ></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"></td>
                                            <td style="background-color:yellow"><b><?php $res = $entrerDecembre - $sortirDecembre ; echo $res ;  ?></b></td>
                                            <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                          </tr>

                                         <?php
                                    }else{
                                      echo '<tr><td colspan="7" style="padding-left:450px">Aucun compte pour mois de decembre</td></tr>';
                                    }
                                  ?>  
                                </tbody>
                                <tbody>
                                <tr><td style="padding-left:450px ; background-color:blue ;" colspan="5"><b>TOTAL </b></td></tr>
                  
                                  <tr><td colspan="2"></td>
                                      <td colspan=""><?php echo $sommeEntre ; ?></td>
                                      <td colspan=""><?php echo $sommeSortie ; ?></td>
                                      <td colspan=""></td>
                                  </tr>
                                  <tr>
                                       <td colspan="3"></td>
                                       <td style="background-color:yellow"><b><?php $res = $sommeEntre - $sommeSortie ; echo $res ;  ?></b></td>
                                       <td> <b><?php if($res < 0 ){echo 'PERTE' ; }else if($res > 0){ echo 'BENEFICE' ; }?></b></td>
                                    </tr>
                                </tbody>
                               </table>

                         </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->   
      </div>
    </div>
    </div>

  </body> 
 
 
<!-- Add new modal -->
<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
      <h4 class="modal-title" id="myModalLabel">Ajout d'un nouveau client</h4>
        <button type="button" class="close fermerAdd" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div id="notifAdd">
        </div>
        <p>Veuillez remplir ce formulaire pour ajouter un client</p>

        <form id="usineForm" method="post"> 
          <div class="form-group">
            <i id="inom" style="color:red"></i>
            <input type="text" class="form-control" id="nomCli" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="iadresse" style="color:red"></i>
            <input type="text" class="form-control" id="adresseCli" placeholder="Adresse">
          </div>
          <div class="form-group">
            <i id="itelephone" style="color:red"></i>
            <input type="text" class="form-control" id="telephoneCli" placeholder="Telephone">
          </div>          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerAdd" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary" id="saveCli" style="font-size:15px">Ajouter</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>



 <!-- Edit modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-xs">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edition d'un client</h4>
        <button type="button" class="close fermerEdit" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <div class="notifUpdate"></div>          
        <p>Modifier la valeur du champ pour editer un client</p>
        <form id="editForm" method="post"> 
          <div id="notifEdit"></div>
          <div class="form-group">
            <input type="hidden" class="form-control" id="idCliEdit" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="inomCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="nomCliEdit" placeholder="Nom " >
          </div>
          <div class="form-group">
            <i id="iadresseCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="adresseCliEdit" placeholder="Adresse">
          </div>
          <div class="form-group">
            <i id="itelephoneCliEdit" style="color:red"></i>
            <input type="text" class="form-control" id="telephoneCliEdit" placeholder="Telephone">
          </div> 

      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default fermerEdit" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Fermer</button>
           <button type="button" class="btn btn-primary " id="updateCli" style="font-size:15px">Modifier</button>          
        </div>
      </div>
        </form>

    </div>
  </div>
</div>

<!-- Delete modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Suppression</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        
      </div>
      <div class="modal-body">
        <p id="titreSuppr"></p>
        <input type="hidden" id="idCliDelete">          
      </div>
      <div class="modal-footer">
        <div>
           <button type="button" class="btn btn-default" data-dismiss="modal" style="border:1px solid #dadada  ; font-size:15px">Annuler</button>
           <button type="button" class="btn btn-primary" id="deleteCli"  data-dismiss="modal" style="font-size:15px">Supprimer</button>          
        </div>
      </div>

    </div>
  </div>
</div>

<?php include('layout/footer.php') ; ?>
<?php include('ajax/client/ajax_client.php') ; ?>
<?php include('ajax/client/controlChampClient.php') ; ?>