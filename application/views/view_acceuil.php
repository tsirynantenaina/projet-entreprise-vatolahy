
<?php include('layout/header.php') ; ?>
<?php $visiter = $page ; ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu-->
          <?php include('layout/menu.php') ; ?>
        <!-- menu -->

        <!-- /top navigation -->
        <?php include('layout/navigation.php') ; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row" style="display: inline-block;" >
          <div class="tile_count" >
            <div class="col-md-2 col-sm-4  tile_stats_count" style="margin-left:-10px ; width:300px ; text-align:center">
              <span class="count_top"><i class=""></i> Total Client</span>
              <div class="count"><?php echo $nombreClient ; ?></div> 
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width:300px ; text-align:center">
              <span class="count_top"><i class=""></i> Commande en attente </span>
              <div class="count"><?php echo $nombreAttente; ?></div>   
            </div>
             <div class="col-md-2 col-sm-4  tile_stats_count" style="width:300px ; text-align:center">
              <span class="count_top"><i class=""></i> A livrér aujourdhi</span>
              <div class="count"><?php echo $livraison; ?></div>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width:300px ; text-align:center">
              <span class="count_top"><i class=""></i>Produits vendus du jour</span>
              <div class="count "><?php echo $achatJour; ?></div>
              
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width:300px ; text-align:center">
              <span class="count_top"><i class=""></i> Commande du jour </span>
              <div class="count"><?php echo $commandeJour; ?></div>
            </div>
            <div class="col-md-2 col-sm-4  tile_stats_count" style="width:300px ; text-align:center">
              <span class="count_top"><i class=""></i>Variété de produits</span>
              <div class="count"><?php echo $nombreProduit; ?></div>
            </div>
          </div>
        </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Graphe<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Representation graphique</a>
                      </li>
                      
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" style="height:330px;" id="home" role="tabpanel" aria-labelledby="home-tab">
                         <div class="x_panel">
                            <div id="mainb" style="height:300px;"></div>
                         </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <br />
        </div>
        <!-- /page content -->
      </div>
    </div>
  </body> 
  <?php include('layout/footer.php') ; ?>
<?php include('layout/graphe.php') ; ?>
        