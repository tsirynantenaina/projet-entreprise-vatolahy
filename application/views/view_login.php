<?php include('layout/header.php') ; ?>
<body style="background-color:#fafafa">
  <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
          <div class="animate form login_form">
            <section class="login_content">
              <form method="" action="">
                <h1>Authentification</h1>
                <div id="messageAlert"></div>
                <div>
                  <i id="ipseudo" style="color:red"></i>
                  <input style="padding-left:18px ; color:black" type="text" name="pseudo" id="pseudo" class="form-control" placeholder="Utilisateur"  />
                </div>
                <div>
                  <i id="imdp" style="color:red"></i>
                  <input style="padding-left:18px ; color:black" type="password" name="mdp" id="mdp" class="form-control" placeholder="Mot de passe" /> 
                </div> 
                <!--<div>
                    <select class="form-control" name="type" id="type" style="border-radius:3px 1px 1px 3px">
                       <option>administrateur</option>
                       <option>simple utilisateur</option>
                    </select>
                </div><br>-->
                <div>
                   <button class="btn btn-primary btn-block btnConnecter" id="btnConnecter" style="focus:none" type="button" >Se connecter</button>
                   <!--<a class="reset_pass" href="#">Mot de passe oublié?</a>  --><br>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                  
                </div>
              </form>
            </section>
          </div>
        </div>
   </div>
</body>

<?php include('layout/footer.php') ; ?>
<?php include('ajax/login/ajax_login.php') ; ?>
<?php include('ajax/login/controlChamp.php') ; ?>