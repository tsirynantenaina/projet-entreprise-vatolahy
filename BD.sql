-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  sam. 03 juil. 2021 à 16:07
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `vatolahy`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

DROP TABLE IF EXISTS `achat`;
CREATE TABLE IF NOT EXISTS `achat` (
  `idCli` int(11) DEFAULT NULL,
  `idProd` int(11) DEFAULT NULL,
  `qteAchat` int(11) DEFAULT NULL,
  `subtotalAchat` int(11) DEFAULT NULL,
  `dateAchat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `factureAchat` int(11) DEFAULT NULL,
  KEY `idCli` (`idCli`),
  KEY `idProd` (`idProd`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `achat`
--

INSERT INTO `achat` (`idCli`, `idProd`, `qteAchat`, `subtotalAchat`, `dateAchat`, `factureAchat`) VALUES
(6, 1, 2, 260000, '2021-06-02 15:54:12', 1),
(7, 1, 2, 260000, '2021-06-02 15:54:38', 2),
(7, 2, 2, 40000, '2021-06-02 15:54:38', 2),
(7, 1, 2, 260000, '2021-06-02 15:54:53', 3),
(9, 1, 1, 130000, '2021-06-02 16:22:00', 4),
(10, 1, 2, 260000, '2021-06-02 16:54:50', 5),
(11, 1, 10, 1300000, '2021-06-03 11:58:45', 6),
(11, 2, 4, 80000, '2021-06-03 11:58:45', 6),
(11, 1, 2, 260000, '2021-06-03 13:36:27', 7),
(11, 2, 2, 40000, '2021-06-03 13:36:28', 7),
(9, 1, 8, 1040000, '2021-06-03 13:38:54', 8),
(9, 2, 8, 160000, '2021-06-03 13:38:54', 8),
(8, 1, 2, 260000, '2021-06-03 13:40:02', 9),
(8, 2, 2, 40000, '2021-06-03 13:40:02', 9),
(10, 1, 3, 390000, '2021-06-03 13:57:35', 10),
(NULL, 2, 2, 40000, '2021-06-03 15:37:04', 11),
(11, 1, 2, 260000, '2021-06-03 15:40:56', 12),
(12, 1, 2, 260000, '2021-06-03 15:43:04', 13),
(12, 1, 2, 260000, '2021-06-07 06:57:27', 14),
(12, 2, 2, 40000, '2021-06-07 06:57:27', 14),
(13, 1, 2, 260000, '2021-06-11 12:26:53', 15),
(13, 1, 2, 260000, '2021-06-15 06:24:20', 16),
(13, 2, 2, 60000, '2021-06-15 06:24:20', 16),
(11, 1, 2, 260000, '2021-06-15 06:27:13', 17),
(11, 2, 2, 60000, '2021-06-15 06:27:13', 17),
(13, 1, 1, 130000, '2021-06-15 06:42:38', 18),
(13, 1, 2, 260000, '2021-06-15 07:58:36', 19),
(12, 1, 3, 390000, '2021-06-15 08:00:46', 20),
(10, 1, 2, 260000, '2021-06-15 08:25:10', 21),
(8, 1, 3, 390000, '2021-06-16 14:00:56', 22),
(8, 2, 2, 60000, '2021-06-16 14:00:56', 22),
(14, 3, 3, 600000, '2021-06-17 09:41:49', 23),
(14, 2, 3, 90000, '2021-06-17 09:41:49', 23),
(8, 1, 2, 260000, '2021-07-01 15:44:52', 24),
(8, 2, 2, 60000, '2021-07-01 15:44:52', 24);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `idCli` int(11) NOT NULL AUTO_INCREMENT,
  `nomCli` varchar(100) DEFAULT NULL,
  `adresseCli` varchar(100) DEFAULT NULL,
  `telephoneCli` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idCli`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idCli`, `nomCli`, `adresseCli`, `telephoneCli`) VALUES
(6, 'Dr liva', 'Antambohobe', '0348874455'),
(7, 'Majusse', 'Antanambao', '0348874456'),
(8, 'Rakoto', 'Antambohobe', '0348874456'),
(9, 'lita', 'Antambohobe', '0349900044'),
(10, 'Randria', 'Mahamanina', '0349900003'),
(11, 'sitraka', 'Andrainjato', '0345566709'),
(12, 'koto', 'Ampasambzaha', '0348874456'),
(13, 'Bryce', 'Talatamaty', '0346481364'),
(14, 'Mr haja', 'Fianarantsoa', '0348860690');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `idCom` int(11) NOT NULL AUTO_INCREMENT,
  `idCli` int(11) DEFAULT NULL,
  `commande` text,
  `avance` int(11) DEFAULT NULL,
  `Net` int(11) DEFAULT NULL,
  `reste` int(11) DEFAULT NULL,
  `dateLivraison` date DEFAULT NULL,
  `dateCom` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emargement` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sommeRecu` int(11) DEFAULT NULL,
  `jourLivrance` date DEFAULT NULL,
  PRIMARY KEY (`idCom`),
  KEY `idCli` (`idCli`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`idCom`, `idCli`, `commande`, `avance`, `Net`, `reste`, `dateLivraison`, `dateCom`, `emargement`, `sommeRecu`, `jourLivrance`) VALUES
(13, 13, 'Betton', 50000, 100000, 0, '2021-08-01', '2021-06-11 10:28:04', 'LIVRE-PAYE', 100000, '2021-06-11'),
(14, 11, 'Ballustre', 80000, 100000, 0, '2022-01-01', '2021-06-11 10:30:57', 'LIVRE-PAYE', 100000, '2021-06-11'),
(15, 13, '10 ballustre', 800000, 1000000, 0, '2021-09-02', '2021-06-15 06:34:42', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(16, 11, 'Dall', 500000, 1000000, 0, '2021-08-02', '2021-06-15 06:43:54', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(17, 9, '10 dall', 70000, 100000, 0, '2021-11-09', '2021-06-15 07:06:07', 'LIVRE-PAYE', 100000, '2021-06-15'),
(18, 13, '10BALLUSTRE', 70000, 100000, 0, '2021-09-01', '2021-06-15 07:20:35', 'LIVRE-PAYE', 100000, '2021-06-15'),
(19, 8, 'Ballustre', 700000, 1000000, 0, '2021-09-01', '2021-06-15 08:02:01', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(20, 11, '1000 briquette', 500000, 1000000, 0, '2021-07-01', '2021-06-15 08:03:44', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(21, 12, '1000 dall', 600000, 1000000, 0, '2021-09-01', '2021-06-15 08:26:06', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(22, 13, 'hfyf', 500000, 1000000, 0, '2021-08-01', '2021-06-15 11:08:22', 'LIVRE-PAYE', 1000000, '2021-06-15'),
(23, 11, '1000 briquette moderne', 5000000, 10000000, 0, '2021-08-01', '2021-06-15 11:12:02', 'LIVRE-PAYE', 10000000, '2021-06-17'),
(24, 14, 'betton 50cm', 50000, 100000, 50000, '2021-10-01', '2021-06-17 09:44:23', 'LIVRE-NON PAYE', 50000, '2021-06-17'),
(25, 14, '10 buz', 500000, 1000000, 500000, '2021-08-10', '2021-07-01 15:45:48', 'EN ATTENTE', 500000, NULL),
(26, 10, '17 ballustre', 1000000, 2000000, 1000000, '2021-07-01', '2021-07-01 15:49:00', 'EN ATTENTE', 1000000, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `idCompte` int(11) NOT NULL AUTO_INCREMENT,
  `action` text,
  `operation` text,
  `sommeCompte` int(11) DEFAULT NULL,
  `dateCompte` date DEFAULT NULL,
  `sommeEntre` int(11) DEFAULT NULL,
  `factureEntre` text,
  PRIMARY KEY (`idCompte`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`idCompte`, `action`, `operation`, `sommeCompte`, `dateCompte`, `sommeEntre`, `factureEntre`) VALUES
(24, 'DEPENSE', 'JIRAMA', 500000, '2021-06-15', NULL, NULL),
(25, 'DEPENSE', 'SALAIRE EMPLOYE', 2000000, '2021-06-15', NULL, NULL),
(26, 'ACHAT', NULL, NULL, '2021-06-15', 260000, 'achat N°21'),
(27, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-06-15', 600000, 'commande N°21'),
(28, 'COMMANDE (reste)', 'PAYEMENT RESTE COMMANDE', NULL, '2021-06-15', 400000, 'commande N°21'),
(29, 'COMMANDE (reste)', 'PAYEMENT RESTE COMMANDE', NULL, '2021-06-15', 30000, 'commande N°18'),
(30, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-06-15', 500000, 'commande N°22'),
(31, 'COMMANDE (reste)', 'PAYEMENT RESTE COMMANDE', NULL, '2021-06-15', 500000, 'commande N°22'),
(32, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-06-15', 5000000, 'commande N°23'),
(33, 'ACHAT', NULL, NULL, '2021-06-16', 450000, 'achat N°22'),
(34, 'ACHAT', NULL, NULL, '2021-06-17', 690000, 'achat N°23'),
(35, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-06-17', 50000, 'commande N°24'),
(36, 'COMMANDE (reste)', 'PAYEMENT RESTE COMMANDE', NULL, '2021-06-17', 5000000, 'commande N°23'),
(37, 'ACHAT', NULL, NULL, '2021-07-01', 320000, 'achat N°24'),
(38, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-07-01', 500000, 'commande N°25'),
(39, 'DEPENSE', 'JIRAMA Juillet', 5000000, '2021-07-01', NULL, NULL),
(40, 'COMMANDE (avance)', 'PAYEMENT AVANCE COMMANDE', NULL, '2021-07-01', 1000000, 'commande N°26');

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `idProd` int(11) DEFAULT NULL,
  `qtePanier` int(11) DEFAULT NULL,
  `subtotalPanier` int(11) DEFAULT NULL,
  KEY `idProd` (`idProd`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`idProd`, `qtePanier`, `subtotalPanier`) VALUES
(1, 1, 130000),
(2, 1, 30000);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `idProd` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `pu` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProd`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`idProd`, `designation`, `stock`, `pu`) VALUES
(1, 'ballustre', 281, 130000),
(2, 'briquette', 252, 30000),
(3, 'Buz', 97, 200000);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(100) DEFAULT NULL,
  `mdp` varchar(100) DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idUser`, `pseudo`, `mdp`, `type`) VALUES
(1, 'admin', 'admin', 'administrateur');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
